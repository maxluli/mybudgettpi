<?php
//     __  ___         _            ___       __               
//    /  |/  ___ __ __(___ _ ___   / _ \___  / /  __ _ ___ ____
//   / /|_/ / _ `\ \ / /  ' / -_) / , _/ _ \/ _ \/  ' / -_/ __/
//  /_/  /_/\_,_/_\_/_/_/_/_\__/ /_/|_|\___/_//_/_/_/_\__/_/     
//                                                                                       
//  Nom Du projet : MyBudget
//  Developpeur : Maxime.rhmr
//  Version : 1.0
//  Date de release : 2021.05.20
//  Maitre enseignant : Mr Garchery Stephane
//  Experts : Mr Terrond Nicolas,Mr Strazzery Mickael
//  Fichier : operations.php

//prérequis
session_start();
require("../php/fonctions.php");
//prérequis

//     ___  __  ____________  ___  ___________ ______________  _  ______
//    / _ |/ / / /_  __/ __ \/ _ \/  _/ __/ _ /_  __/  _/ __ \/ |/ / __/
//   / __ / /_/ / / / / /_/ / , __/ /_\ \/ __ |/ / _/ // /_/ /    _\ \  
//  /_/ |_\____/ /_/  \____/_/|_/___/___/_/ |_/_/ /___/\____/_/|_/___/  
//                                                                                                               
//  Seuls autorisés : utilisateurs connectés et non admins

//est ce que le user est bien connecté
if (isset($_SESSION["user"])) {
    //est ce que le user est bien non admin
    if ($_SESSION["admin"] == 0) {
    } else {
        header("location:login.php");
    }
} else {
    header("location:login.php");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Head de la documentation bootstrap -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!-- Boostrap JS -->
    <script src="../js/bootstrap.bundle.min.js"></script>
    <!-- Mon CSS -->
    <link href="../css/monCss.css" rel="stylesheet">
    <!-- Icons -->
    <link href="../css/all.css" rel="stylesheet">
    <title>operations</title>
</head>

<body style='background-color:#181818;color:#FFFFFF'>
    <div class="main" style="margin:0px">
        <?php include("../php/nav.php"); ?>

        <div class="text-center" style='padding-top:0;'>
            <h1>Operations</h1>
        </div>
        <h2 class="text-center"><a href="ajoutOperation.php" style='color:#AAAAAA;text-decoration:none'><button type="button" class="btn btn-light" style="background-color:#212121;color:#FFFFFF">Ajouter une operation</button></a></h2>

        <?php
        //Affichage des differentes operations du user
        DisplayOperationsByUser($_SESSION["idUser"], 150)
        ?>
    </div>
    <?php include("../php/footer.php"); ?>
</body>

</html>