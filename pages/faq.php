<?php
//     __  ___         _            ___       __               
//    /  |/  ___ __ __(___ _ ___   / _ \___  / /  __ _ ___ ____
//   / /|_/ / _ `\ \ / /  ' / -_) / , _/ _ \/ _ \/  ' / -_/ __/
//  /_/  /_/\_,_/_\_/_/_/_/_\__/ /_/|_|\___/_//_/_/_/_\__/_/     
//                                                                                       
//  Nom Du projet : MyBudget
//  Developpeur : Maxime.rhmr
//  Version : 1.0
//  Date de release : 2021.05.20
//  Maitre enseignant : Mr Garchery Stephane
//  Experts : Mr Terrond Nicolas,Mr Strazzery Mickael
//  Fichier : disconnect.php

//prérequis
session_start();
require("../php/fonctions.php");
//prérequis

//     ___  __  ____________  ___  ___________ ______________  _  ______
//    / _ |/ / / /_  __/ __ \/ _ \/  _/ __/ _ /_  __/  _/ __ \/ |/ / __/
//   / __ / /_/ / / / / /_/ / , __/ /_\ \/ __ |/ / _/ // /_/ /    _\ \  
//  /_/ |_\____/ /_/  \____/_/|_/___/___/_/ |_/_/ /___/\____/_/|_/___/  
//                                                                                                               
//  Seuls autorisés : tout le monde
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Head de la documentation bootstrap -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!-- Boostrap JS -->
    <script src="../js/bootstrap.bundle.min.js"></script>
    <!-- Mon CSS -->
    <link href="../css/monCss.css" rel="stylesheet">
    <!-- Icons -->
    <link href="../css/all.css" rel="stylesheet">
    <title>faq</title>
</head>

<body style='background-color:#181818;color:#FFFFFF'>
    <div class="main">
        <?php include("../php/nav.php"); ?>
        <div class="text-center" style='padding-top:0'>
            <h1><b>F</b>oire <b>A</b>ux <b>Q</b>uestions</h1>
        </div>
        <div class="container">
            <h3>General</h3>

            <div class="accordion accordion-flush" id="accordionFlushExample">
                <div class="accordion-item">
                    <h2 class="accordion-header" id="flush-headingOne">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTitle" aria-expanded="false" aria-controls="flush-collapseOne">
                            Qu'est ce que MyBudget ?
                        </button>
                    </h2>
                    <div id="flush-collapseTitle" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                        <div class="accordion-body">
                            <p>MyBudget est une application web exclusivement qui as été developpée par Maxime Rohmer dans le cadre de son <b>T</b>ravail <b>P</b>ratique <b>I</b>ndividuel (TPI)</p>
                            <p>Cette application a pou but de servir de gestionnaire de budgets et d'économies</p>
                            <p>Elle permet une gestion de differents comptes en banques et budgets et d'y ajouter des entrées et sorties sous formes d'opérations</p>
                        </div>
                    </div>
                </div>
            </div>
            <?php

            if (isset($_SESSION["user"]) && $_SESSION["user"] != "") {
                if ($_SESSION["admin"] == 1) {
                    //La page de FAQ des admins
                    echo '
                        <h3>Questions Administrations</h3>
                        <div class="accordion accordion-flush" id="accordionFlushExample" style="">
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingOne">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                                        Comment ajouter des mots clés ?
                                    </button>
                                </h2>
                                <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                        <p>Pour ajouter des mots clés c est très simple , il suffit de se rendre sur <a href="ajoutMotClef.php">la page d ajout</a></p>
                                        <p><img src="../img/KeyWord.PNG" class="class="img-fluid"></img></p>
                                        <p>Vous aurez simplement a remplir les champs et de créer le mot clé</p>
                                    </div>
                                </div>
                            </div>

                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingOne">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseOne">
                                        Comment supprimer ou modifier un mot clé
                                    </button>
                                </h2>
                                <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                        <p>Rendez vous sur votre <a href="dashboardAdmin.php">page d administration</a> et ensuite vous avez les boutons modifier et supprimer </p>     
                                        <p><img src="../img/KeyWordModif.PNG" class="class="img-fluid"></img></p>       
                                    </div>
                                </div>
                            </div>

                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingOne">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseOne">
                                        Comment me déconnecter ?
                                    </button>
                                </h2>
                                <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                        <p>Sur votre navigation</p>
                                        <p><img src="../img/NavAdmin.PNG" class="class="img-fluid"></img></p>
                                    </div>
                                </div>
                            </div>
                        </div>               
                            ';
                } else {
                    //La page de FAQ des utilisateurs connectés
                    echo '
                    <h3>Questions Utilisateur</h3>
                    <p>Rappel general , presque toutes les opérations sont accessibles depuis la page de dashBoard , les boutons oranges correspondent a des boutons de modification et les boutons rouges des boutons de supression</p>
                    <div class="accordion accordion-flush" id="accordionFlushExample" style="">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingOne">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                                    Comment créer un compte ?
                                </button>
                            </h2>
                            <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">
                                <p>Rendez vous sur <a href="ajoutCompte.php">la page d ajout de compte</a></p>
                                <p>Vous aurez simplement a remplir les champs comme indiqué</p>
                                <p><img src="../img/CreateCompte.PNG" class="class="img-fluid"></img></p>
                            </div>
                        </div>
                    </div>
                    </div>

                    <div class="accordion accordion-flush" id="accordionFlushExample" style="">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingOne">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseOne">
                                    Comment créer un budget ?
                                </button>
                            </h2>
                            <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">
                                <p>Rendez vous sur <a href="ajoutBudget.php">la page d ajout de budget</a></p>
                                <p>Vous aurez simplement a remplir les champs comme indiqué</p>
                                <p><img src="../img/CreateBudget.PNG" class="class="img-fluid"></img></p>
                            </div>
                        </div>
                    </div>
                    </div>

                    <div class="accordion accordion-flush" id="accordionFlushExample" style="">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingOne">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseOne">
                                    Comment créer une opération ?
                                </button>
                            </h2>
                            <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">
                                <p>Rendez vous sur <a href="ajoutOperation.php">la page d ajout d opération</a></p>
                                <p>Vous aurez simplement a remplir les champs comme indiqué</p>
                                <p><img src="../img/CreateOperation.PNG" class="class="img-fluid"></img></p>
                            </div>
                        </div>
                    </div>
                    </div>

                    <div class="accordion accordion-flush" id="accordionFlushExample" style="">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingOne">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFour" aria-expanded="false" aria-controls="flush-collapseOne">
                                    Comment modifier ou supprimer un compte?
                                </button>
                            </h2>
                            <div id="flush-collapseFour" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">
                                <p>Rendez vous sur <a href="comptes.php">la page comptes</a> ou sur <a href="dashboard.php">votre dashboard</a> et utilisez les boutons modifier/suprimmer</p>
                                <p><img src="../img/Comptes.PNG" class="class="img-fluid"></img></p>
                                Ensuite le formulaire de modification fonctionne comme celui de création (voir : comment creer un compte?)
                            </div>
                        </div>
                    </div>
                    </div>
                        
                    <div class="accordion accordion-flush" id="accordionFlushExample" style="">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingOne">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFive" aria-expanded="false" aria-controls="flush-collapseOne">
                                    Comment modifier ou supprimer un budget?
                                </button>
                            </h2>
                            <div id="flush-collapseFive" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">
                                <p>Rendez vous sur <a href="budgets.php">la page budget</a> ou sur <a href="dashboard.php">votre dashboard</a> et utilisez les boutons modifier/suprimmer</p>
                                <p><img src="../img/Budgets.PNG" class="class="img-fluid"></img></p>
                                Ensuite le formulaire de modification fonctionne comme celui de création (voir : comment creer un budget?)
                            </div>
                        </div>
                    </div>
                    </div>  

                    <div class="accordion accordion-flush" id="accordionFlushExample" style="">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingOne">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseSix" aria-expanded="false" aria-controls="flush-collapseOne">
                                    Comment modifier ou supprimer une opération?
                                </button>
                            </h2>
                            <div id="flush-collapseSix" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">
                                <p>Rendez vous sur <a href="operations.php">la page operations</a> ou sur <a href="dashboard.php">votre dashboard</a> et utilisez les boutons modifier/suprimmer</p>
                                <p><img src="../img/Operations.PNG" class="class="img-fluid"></img></p>
                                Ensuite le formulaire de modification fonctionne comme celui de création (voir : comment creer une opération?)
                            </div>
                        </div>
                    </div>
                    </div>  
                    <div class="accordion accordion-flush" id="accordionFlushExample" style="">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingOne">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseSeven" aria-expanded="false" aria-controls="flush-collapseOne">
                                    Comment accèder aux informations développées de mes comptes,budgets ou opérations ?
                                </button>
                            </h2>
                            <div id="flush-collapseSeven" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">
                                <p>Dans votre navigation il y a un lien vers la page dtaillée de ces 3 choses donc cliquez sur celle qui vous intéresse et sachez que sur <a href="dashboard.php">votre dashboard</a> vous avez un condensé de ces informations</p>
                                <p><img src="../img/Nav.PNG" class="class="img-fluid"></img></p>
                            </div>
                        </div>
                    </div>
                    </div>   

                            ';
                }
            } else {
                //La page de FAQ pour les utilisateurs non connectés
                echo '
                <h3>Questions Visiteurs</h3>
                <div class="accordion accordion-flush" id="accordionFlushExample" style="">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingOne">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                                Comment me créer un compte ?
                            </button>
                        </h2>
                        <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body">
                                <p>Rendez vous sur <a href="register.php">la page d enregistrement</a></p>
                                <p>Vous aurez simplement a remplir les champs comme indiqué</p>
                                <p><img src="../img/Login.PNG" class="class="img-fluid"></img></p>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingOne">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseOne">
                                Comment me connecter ?
                            </button>
                        </h2>
                        <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body">
                                <p>Rendez vous sur <a href="login.php">la page de login</a></p>
                                <p>Vous devez simplement entrer vos identifiants</p>
                                <p><img src="../img/Register.PNG" class="class="img-fluid"></img></p>
                            </div>
                        </div>
                    </div>




                </div>
                    ';
            }
            ?>
        </div>
    </div>
    <?php include("../php/footer.php"); ?>
</body>

</html>