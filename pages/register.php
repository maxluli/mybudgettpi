<?php
//     __  ___         _            ___       __               
//    /  |/  ___ __ __(___ _ ___   / _ \___  / /  __ _ ___ ____
//   / /|_/ / _ `\ \ / /  ' / -_) / , _/ _ \/ _ \/  ' / -_/ __/
//  /_/  /_/\_,_/_\_/_/_/_/_\__/ /_/|_|\___/_//_/_/_/_\__/_/     
//                                                                                       
//  Nom Du projet : MyBudget
//  Developpeur : Maxime.rhmr
//  Version : 1.0
//  Date de release : 2021.05.20
//  Maitre enseignant : Mr Garchery Stephane
//  Experts : Mr Terrond Nicolas,Mr Strazzery Mickael
//  Fichier : register.php

//prérequis
session_start();
require("../php/fonctions.php");
//prérequis

//     ___  __  ____________  ___  ___________ ______________  _  ______
//    / _ |/ / / /_  __/ __ \/ _ \/  _/ __/ _ /_  __/  _/ __ \/ |/ / __/
//   / __ / /_/ / / / / /_/ / , __/ /_\ \/ __ |/ / _/ // /_/ /    _\ \  
//  /_/ |_\____/ /_/  \____/_/|_/___/___/_/ |_/_/ /___/\____/_/|_/___/  
//                                                                                                               
//  Seuls autorisés : Tout le monde

//la variable message contiendra tout les differents messages d'indication d'erreur 
$Message = "";
//set initial des variables 
$email = "";
$password1 = "";
$password2 = "";
//on verifie si un formulaire as été envoyé
if (isset($_POST["submitButton"])) {
    //le formulaire as été envoyé on peut donc traiter les datas
    //récupération des variables filtrées
    $email = filter_input(INPUT_POST, "email", FILTER_SANITIZE_EMAIL);
    $password1 = filter_input(INPUT_POST, "pass1", FILTER_SANITIZE_STRING);
    $password2 = filter_input(INPUT_POST, "pass2", FILTER_SANITIZE_STRING);
    //verifiaction des non nuls
    if ($email != "" && $password1 != "" && $password2 != "") {
        //toutes les variables actuelles sont complètes et filtrées 
        //verification que les deux mots de passes sont identiques
        if ($password1 != $password2) {
            $Message .= "<div class='alert alert-danger' role='alert'>
            Les deux mots de passes doivent être identiques p1 : " . $password1 . " p2 : " . $password2 . "
            </div>";
        } else {
            if (strlen($password1) >= 8) {
                if (Exist($email)) {
                    $Message .= "<div class='alert alert-danger' role='alert'>
                    Cet utilisateur existe deja
                    </div>";
                } else {
                    //toutes les infos sont prètes on peut donc insèrer le nouvel utilisateur
                    if (CreateUser($email, $password1)) {
                        $Message .= "<div class='alert alert-success' role='alert'>
                    inscription reussie vous allez être redirigé dans quelques instants
                    </div>";
                        header("location:login.php");
                    } else {
                        $Message .= "<div class='alert alert-danger' role='alert'>
                    L'utilisateur n'as pas pu être créé, pour rappel un login ne peut pas faire plus de 45 caractères
                    </div>";
                    }
                }
            } else {
                $Message .= "<div class='alert alert-danger' role='alert'>
                Votre mot de passe doit être plus long
                </div>";
            }
        }
    } else {
        $Message .= "<div class='alert alert-danger' role='alert'>
        Veuillez remplir tout les champs du formulaire
        </div>";
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Head de la documentation bootstrap -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!-- Boostrap JS -->
    <script src="../js/bootstrap.bundle.min.js"></script>
    <!-- Mon CSS -->
    <link href="../css/monCss.css" rel="stylesheet">
    <!-- Icons -->
    <link href="../css/all.css" rel="stylesheet">
    <title>Register</title>
</head>

<body style='background-color:#181818;color:#FFFFFF'>
    <div class="main">
        <?php include("../php/nav.php"); ?>
        <div class="text-center" style='padding-top:3%'>
            <h1>S'enregistrer</h1>
        </div>
        <div class='container rounded' style='background-color:#212121;margin-top:4%;padding-left:8%;padding-right:8%;padding-top:2.5%;padding-bottom:2.5%;width:40%'>
            <h1>Register</h1>
            <form action="#" method="POST">
                <label for="email" class="form-label">Email</label>
                <input type='email' name='email' required='true' placeholder="" class="form-control" value="<?= $email ?>"></b>
                <div id="emailHelpBlock" class="form-text">
                    Exemple Test@gmail.com
                </div>
                <label for="pass1" class="form-label">Password</label>
                <input type="password" required="true" name="pass1" placeholder="" class="form-control">
                <div id="passwordHelpBlock" class="form-text">
                    Attention le mot de passe doit faire plus de 8 caractères
                </div>
                <label for="pass2" class="form-label">Re-password</label>
                <input type="password" required="true" name="pass2" placeholder="" class="form-control">
                <div id="passwordHelpBlock" class="form-text">
                    Vous devez confirmez votre mdp
                </div>
                <div class="text-center" style='margin-top:5%'>
                    <button type="submit" name="submitButton" class="btn btn-secondary btn-lg form-control" style="width:50%; min-width:80px">Register</button></br>
                    Vous avez déja un compte ? <a href="login.php" style="text-decoration:none;color:#AAAAAA"> Se connecter</a>
                </div>

            </form>
            <?php
            if ($Message != "") {
                echo "<div class='text-center' style='margin-top:5%'>
                $Message
            </div>";
            }
            $Message
            ?>
        </div>
    </div>
    <?php include("../php/footer.php"); ?>
</body>

</html>