<?php
//     __  ___         _            ___       __               
//    /  |/  ___ __ __(___ _ ___   / _ \___  / /  __ _ ___ ____
//   / /|_/ / _ `\ \ / /  ' / -_) / , _/ _ \/ _ \/  ' / -_/ __/
//  /_/  /_/\_,_/_\_/_/_/_/_\__/ /_/|_|\___/_//_/_/_/_\__/_/     
//                                                                                       
//  Nom Du projet : MyBudget
//  Developpeur : Maxime.rhmr
//  Version : 1.0
//  Date de release : 2021.05.20
//  Maitre enseignant : Mr Garchery Stephane
//  Experts : Mr Terrond Nicolas,Mr Strazzery Mickael
//  Fichier : modifMotClef.php

//prérequis
session_start();
require("../php/fonctions.php");
//prérequis

//     ___  __  ____________  ___  ___________ ______________  _  ______
//    / _ |/ / / /_  __/ __ \/ _ \/  _/ __/ _ /_  __/  _/ __ \/ |/ / __/
//   / __ / /_/ / / / / /_/ / , __/ /_\ \/ __ |/ / _/ // /_/ /    _\ \  
//  /_/ |_\____/ /_/  \____/_/|_/___/___/_/ |_/_/ /___/\____/_/|_/___/  
//                                                                                                               
//  Seuls autorisés : utilisateurs connectés et admins

//est ce que le user est connecté
if (isset($_SESSION["user"])) {
    //est ce que le user est bien un admin
    if ($_SESSION["admin"] == 1) {
        //on peut rester sur cette page 
        //la variable message contiendra tout les differents messages d'indication d'erreur 
        $Message = "";
        //set initial des variables 
        
        $id = filter_input(INPUT_GET,"id",FILTER_SANITIZE_NUMBER_INT);
        $result = GetKeyWordById($id);
        if($result != null){
            $name = $result[0]["libele"];
        }else{
            $name = "";
        }
        //on verifie si un formulaire as été envoyé
        if (isset($_POST["submitButton"])) {
            //le formulaire as été envoyé on peut donc traiter les datas
            //recuperation des variables filtrées
            $name = filter_input(INPUT_POST, "name", FILTER_SANITIZE_STRING);

            //on verifie que les deux valeurs ne sont pas vides
            if ($name != "") {
                if (ModifyKeyWord($id,$name)) {
                    $Message .= "<div class='alert alert-successful' role='alert'>
                        Le mot clé as bien été créé'
                        </div>";
                    header("location:dashBoard.php");
                } else {
                    $Message .= "<div class='alert alert-danger' role='alert'>
                        Le mot clé n'as pas pu être créé
                        </div>";
                }
            } else {
                $Message .= "<div class='alert alert-danger' role='alert'>
                Tout les champs du formulaire doivent être remplis
                </div>";
            }
        }
    } else {
        header("location:login.php");
    }
} else {
    header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Head de la documentation bootstrap -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!-- Boostrap JS -->
    <script src="../js/bootstrap.bundle.min.js"></script>
    <!-- Mon CSS -->
    <link href="../css/monCss.css" rel="stylesheet">
    <!-- Icons -->
    <link href="../css/all.css" rel="stylesheet">
    <title>Ajout Mot Clé</title>
</head>

<body style='background-color:#181818;color:#FFFFFF'>
    <div class="main">
        <?php include("../php/nav.php"); ?>
        <div class="text-center" style='padding-top:4%'>
            <h1>Modifier un mot clé</h1>
        </div>

        <div class='container rounded' style='background-color:#212121;margin-top:4%;margin-bottom:4%;padding-left:8%;padding-right:8%;padding-top:5%;padding-bottom:2.5%;width:40%'>
            <h1>Modification mot clé</h1>
            <form action="#" method="POST">
                <label for="email" class="form-label">Nom</label>
                <input type='text' name='name' required='true' placeholder="" class="form-control" value="<?= $name ?>"></b>
                <div id="HelpBlock" class="form-text">
                    Exemple "Urgent" max 45 caractères
                </div>
                <div class="text-center" style='margin-top:5%;'>
                    <button type="submit" name="submitButton" class="btn btn-secondary btn-lg form-control" style="width:50%; min-width:80px">Creer</button></br>
                </div>
            </form>
            <?php
            if ($Message != "") {
                echo "<div class='text-center' style='margin-top:5%'>
                $Message
            </div>";
            }
            ?>
        </div>
    </div>
    <?php include("../php/footer.php"); ?>
</body>

</html>