<?php
//     __  ___         _            ___       __               
//    /  |/  ___ __ __(___ _ ___   / _ \___  / /  __ _ ___ ____
//   / /|_/ / _ `\ \ / /  ' / -_) / , _/ _ \/ _ \/  ' / -_/ __/
//  /_/  /_/\_,_/_\_/_/_/_/_\__/ /_/|_|\___/_//_/_/_/_\__/_/     
//                                                                                       
//  Nom Du projet : MyBudget
//  Developpeur : Maxime.rhmr
//  Version : 1.0
//  Date de release : 2021.05.20
//  Maitre enseignant : Mr Garchery Stephane
//  Experts : Mr Terrond Nicolas,Mr Strazzery Mickael
//  Fichier : modifCOmpte.php

//prérequis
session_start();
require("../php/fonctions.php");
//prérequis

//     ___  __  ____________  ___  ___________ ______________  _  ______
//    / _ |/ / / /_  __/ __ \/ _ \/  _/ __/ _ /_  __/  _/ __ \/ |/ / __/
//   / __ / /_/ / / / / /_/ / , __/ /_\ \/ __ |/ / _/ // /_/ /    _\ \  
//  /_/ |_\____/ /_/  \____/_/|_/___/___/_/ |_/_/ /___/\____/_/|_/___/  
//                                                                                                               
//  Seuls autorisés : utilisateurs connectés et non admins

//est ce que le user est connecté
if (isset($_SESSION["user"])) {
    //est ce que le user n'est bien pas un admin
    if ($_SESSION["admin"] == 0) {
        $id = filter_input(INPUT_GET, "id", FILTER_SANITIZE_NUMBER_INT);
        $result = isAllowedCompte($_SESSION["idUser"], $id);
        //var_dump($_SESSION);
        if ($result != null) {
            $Message = "";
            $name = $result[0]["libele"];
            $number = $result[0]["numero"];
            $bank = $result[0]["banque"];
            //on verifie si un formulaire as été envoyé
            if (isset($_POST["submitButton"])) {
                //le formulaire as été envoyé on peut donc traiter les datas
                //recuperation des variables filtrées
                $name = filter_input(INPUT_POST, "name", FILTER_SANITIZE_STRING);
                $number = filter_input(INPUT_POST, "number", FILTER_SANITIZE_STRING);
                $bank = filter_input(INPUT_POST, "bank", FILTER_SANITIZE_STRING);
                //on verifie que les deux valeurs ne sont pas vides
                if ($name != "" && $number != "" && $bank) {
                    if (strlen($name) <= 45) {
                        if (ModifyAccount($name, $number, $bank, $id)) {
                            $Message .= "<div class='alert alert-successful' role='alert'>
                        Le Compte as bien été créer
                        </div>";
                            header("location:dashBoard.php");
                        } else {
                            $Message .= "<div class='alert alert-danger' role='alert'>
                        Le Compte n'as pas pu être créer
                        </div>";
                        }
                    } else {
                        $Message .= "<div class='alert alert-danger' role='alert'>
                        le nom ne doit pas faire plus de 45 caractères
                        </div>";
                    }
                } else {
                    $Message .= "<div class='alert alert-danger' role='alert'>
                Tout les champs du formulaire doivent être remplis
                </div>";
                }
            }
        }else{
            header("location:login.php");
        }
    } else {
        header("location:login.php");
    }
} else {
    header("location:login.php");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Head de la documentation bootstrap -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!-- Boostrap JS -->
    <script src="../js/bootstrap.bundle.min.js"></script>
    <!-- Mon CSS -->
    <link href="../css/monCss.css" rel="stylesheet">
    <!-- Icons -->
    <link href="../css/all.css" rel="stylesheet">
    <title>Modif Compte</title>
</head>

<body style='background-color:#181818;color:#FFFFFF'>
    <div class="main">
        <?php include("../php/nav.php"); ?>
        <div class="text-center" style='padding-top:3%'>
            <h1>modifier un compte</h1>
        </div>

        <div class='container rounded' style='background-color:#212121;margin-top:4%;padding-left:8%;padding-right:8%;padding-top:2.5%;padding-bottom:2.5%;width:40%'>
            <h1>Modifier compte</h1>
            <form action="#" method="POST">
                <label for="email" class="form-label">Nom</label>
                <input type='text' name='name' required='true' placeholder="" class="form-control" value="<?= $name ?>"></b>
                <div id="HelpBlock" class="form-text">
                    Exemple "Privé" max 45 caractères
                </div>

                <label for="email" class="form-label">Numero de compte</label>
                <input type='text' name='number' required='true' placeholder="" class="form-control" value="<?= $number ?>"></b>
                <div id="HelpBlock" class="form-text">
                    Exemple "255789452652"
                </div>

                <label for="email" class="form-label">Banque</label>
                <input type='text' name='bank' required='true' placeholder="" class="form-control" value="<?= $bank ?>"></b>
                <div id="HelpBlock" class="form-text">
                    Exemple "UBS"
                </div>

                <div class="text-center" style='margin-top:5%;'>
                    <button type="submit" name="submitButton" class="btn btn-secondary btn-lg form-control" style="width:50%; min-width:80px">Modifier</button></br>
                </div>
            </form>
            <?php
            if ($Message != "") {
                echo "<div class='text-center' style='margin-top:5%'>
                $Message
            </div>";
            }
            ?>
        </div>
    </div>
    <?php include("../php/footer.php"); ?>
</body>

</html>