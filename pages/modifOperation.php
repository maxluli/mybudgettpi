<?php
//     __  ___         _            ___       __               
//    /  |/  ___ __ __(___ _ ___   / _ \___  / /  __ _ ___ ____
//   / /|_/ / _ `\ \ / /  ' / -_) / , _/ _ \/ _ \/  ' / -_/ __/
//  /_/  /_/\_,_/_\_/_/_/_/_\__/ /_/|_|\___/_//_/_/_/_\__/_/     
//                                                                                       
//  Nom Du projet : MyBudget
//  Developpeur : Maxime.rhmr
//  Version : 1.0
//  Date de release : 2021.05.20
//  Maitre enseignant : Mr Garchery Stephane
//  Experts : Mr Terrond Nicolas,Mr Strazzery Mickael
//  Fichier : modifOperation.php

//prérequis
session_start();
require("../php/fonctions.php");
//prérequis

//     ___  __  ____________  ___  ___________ ______________  _  ______
//    / _ |/ / / /_  __/ __ \/ _ \/  _/ __/ _ /_  __/  _/ __ \/ |/ / __/
//   / __ / /_/ / / / / /_/ / , __/ /_\ \/ __ |/ / _/ // /_/ /    _\ \  
//  /_/ |_\____/ /_/  \____/_/|_/___/___/_/ |_/_/ /___/\____/_/|_/___/  
//                                                                                                               
//  Seuls autorisés : utilisateurs connectés et non admins

//est ce que le user est connecté
if (isset($_SESSION["user"])) {
    //est ce que le user est bien non admin
    if ($_SESSION["admin"] == 0) {
        $id = filter_input(INPUT_GET, "id", FILTER_SANITIZE_NUMBER_INT);
        $authorisation = isUserAllowedOperation($id, $_SESSION["idUser"]);
        if ($authorisation) {
            $operation = GetOperationById($id);
            if ($operation != null) {
                $Message = "";
                //set initial des variables 
                $name = $operation[0]["libele"];
                $amount = $operation[0]["montant"];
                $idAccount = $operation[0]["comptes_id"];
                $idBudget = $operation[0]["budget_id"];
                $keyWords = GetKeyWordByOperation($id);

                //on verifie si un formulaire as été envoyé
                if (isset($_POST["submitButton"])) {
                    //le formulaire as été envoyé on peut donc traiter les datas
                    //recuperation des variables filtrées
                    $name = filter_input(INPUT_POST, "name", FILTER_SANITIZE_STRING);
                    $amount = filter_input(INPUT_POST, "amount", FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                    $idAccount = filter_input(INPUT_POST, "account", FILTER_SANITIZE_STRING);
                    $idBudget = filter_input(INPUT_POST, "budget", FILTER_SANITIZE_STRING);
                    $keyWords = $_POST["keys"];

                    //on verifie que les deux valeurs ne sont pas vides
                    if ($name != "" && $amount != "" && $idAccount != "") {
                        if ($idBudget == "") {
                            $idBudget = null;
                        }
                        if ($keyWords == null) {
                            //je mets a -1 pour que de l'autre coté quand je fais la fonction d'ajout je sache que il n'y a pas de relations a faire
                            $keyWords = -1;
                        }
                        if (ModifyOperation($name, $amount, $idAccount, $idBudget, $id, $keyWords, $_SESSION["idUser"])) {
                            $Message .= "<div class='alert alert-successful' role='alert'>
                        L'operation as bien été modifiée'
                        </div>";
                            header("location:operations.php");
                        } else {
                            $Message .= "<div class='alert alert-danger' role='alert'>
                        L'operation' n'as pas pu être modifiée
                        </div>";
                        }
                    } else {
                        if ($name == "") {
                            if(!isset($_POST["name"])){
                                $Message .= "<div class='alert alert-danger' role='alert'>
                                Le champ nom ne peut pas etre vide
                                </div>";
                            }else{
                                $Message .= "<div class='alert alert-danger' role='alert'>
                                Le champ nom est invalide
                                </div>";
                            }
                        }
                        if($amount == ""){
                            if(!isset($_POST["amount"])){
                                $Message .= "<div class='alert alert-danger' role='alert'>
                                Le champ montant ne doit pas être vide
                                </div>";
                            }else{
                                $Message .= "<div class='alert alert-danger' role='alert'>
                                Le champ montant est invalide
                                </div>";
                            }
                        }
                        $Message .= "<div class='alert alert-danger' role='alert'>
                        Tout les champs du formulaire doivent être remplis
                        </div>";
                    }
                }
            } else {
                header("location:login.php");
            }
        } else {
            header("location:login.php");
        }
    } else {
        header("location:login.php");
    }
} else {
    header("location:login.php");
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Head de la documentation bootstrap -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!-- Boostrap JS -->
    <script src="../js/bootstrap.bundle.min.js"></script>
    <!-- Mon CSS -->
    <link href="../css/monCss.css" rel="stylesheet">
    <!-- Icons -->
    <link href="../css/all.css" rel="stylesheet">
    <title>Modif Operation</title>
</head>

<body style='background-color:#181818;color:#FFFFFF'>
    <div class="main">
        <?php include("../php/nav.php"); ?>
        <div class="text-center" style='padding-top:3%'>
            <h1>Modifier une opération</h1>
        </div>
        <div class='container rounded' style='background-color:#212121;margin-top:4%;padding-left:8%;padding-right:8%;padding-top:2.5%;padding-bottom:2.5%;width:40%'>
            <h1>Modification opération</h1>
            <form action="#" method="POST">
                <label for="email" class="form-label">Nom</label>
                <input type='text' name='name' required='true' placeholder="" class="form-control" value="<?= $name ?>"></b>
                <div id="HelpBlock" class="form-text">
                    Exemple "courses" max 45 caractères
                </div>
                <label for="email" class="form-label">Montant</label>
                <input type='number' name='amount' required='true' placeholder="" class="form-control" value="<?= $amount ?>"></b>
                <div id="HelpBlock" class="form-text">
                    Exemple "-250"
                </div>
                <label for="account" class="form-label">Compte</label>
                <select class="form-select form-control" aria-label="Choisir un compte" name="account">
                    <option selected></option>
                    <?php
                    $accounts = GetAllAccountsByUser($_SESSION["idUser"]);
                    foreach ($accounts as $a) {
                        if ($a["id"] == $idAccount) {
                            //si il y a un match de budget alors on peut mettre la valeur sticky
                            echo '<option value="' . $a["id"] . '" selected >' . $a["libele"] . '</option>';
                        } else {
                            echo '<option value="' . $a["id"] . '" >' . $a["libele"] . '</option>';
                        }
                    }
                    ?>
                </select>
                <label for="budget" class="form-label">Budget</label>
                <select class="form-select form-control" aria-label="Choisir un compte" name="budget">
                    <option selected></option>
                    <?php
                    $accounts = GetAllBudgetsByUser($_SESSION["idUser"]);
                    foreach ($accounts as $a) {
                        if ($a["id"] == $idBudget) {
                            //si il y a un match de budget alors on peut mettre la valeur sticky
                            echo '<option value="' . $a["id"] . '" selected>' . $a["titre"] . '</option>';
                        } else {
                            echo '<option value="' . $a["id"] . '" >' . $a["titre"] . '</option>';
                        }
                    }
                    ?>
                </select>
                <label for="budget" class="form-label">Mots clefs</label>
                <select class="form-select form-control" aria-label="Choisir un compte" name="keys[]" multiple>
                    <?php
                    if ($keyWords != null) {
                        $Words = GetKeyWords($_SESSION["idUser"]);
                        foreach ($Words as $w) {
                            $found = 0;
                            foreach ($keyWords as $k) {
                                if ($k["id"] == $w["id"]) {
                                    //si il y a un match alors on vas pouvoir la selectionner
                                    $found = 1;
                                }
                            }
                            if ($found == 1) {
                                //il y a eu un match donc elle doit etre mise en "selected"
                                echo '<option selected value="' . $w["id"] . '" >' . $w["libele"] . '</option>';
                            } else {
                                echo '<option value="' . $w["id"] . '" >' . $w["libele"] . '</option>';
                            }
                        }
                    } else {
                        //si il n'y a aucuns mots clés associés 
                        echo '<option selected></option>';
                        $Words = GetKeyWords($_SESSION["idUser"]);
                        foreach ($Words as $w) {
                            echo '<option value="' . $w["id"] . '" >' . $w["libele"] . '</option>';
                        }
                    }
                    ?>
                </select>
                <div class="text-center" style='margin-top:5%;'>
                    <button type="submit" name="submitButton" class="btn btn-secondary btn-lg form-control" style="width:50%; min-width:80px">Modify</button></br>
                </div>
            </form>
            <?php
            if ($Message != "") {
                echo "<div class='text-center' style='margin-top:5%'>
                $Message
            </div>";
            }
            ?>
        </div>
    </div>
    <?php include("../php/footer.php"); ?>
</body>

</html>