<?php
//     __  ___         _            ___       __               
//    /  |/  ___ __ __(___ _ ___   / _ \___  / /  __ _ ___ ____
//   / /|_/ / _ `\ \ / /  ' / -_) / , _/ _ \/ _ \/  ' / -_/ __/
//  /_/  /_/\_,_/_\_/_/_/_/_\__/ /_/|_|\___/_//_/_/_/_\__/_/     
//                                                                                       
//  Nom Du projet : MyBudget
//  Developpeur : Maxime.rhmr
//  Version : 1.0
//  Date de release : 2021.05.20
//  Maitre enseignant : Mr Garchery Stephane
//  Experts : Mr Terrond Nicolas,Mr Strazzery Mickael
//  Fichier : dashBoard.php

//prérequis
session_start();
require("../php/fonctions.php");
//prérequis

//     ___  __  ____________  ___  ___________ ______________  _  ______
//    / _ |/ / / /_  __/ __ \/ _ \/  _/ __/ _ /_  __/  _/ __ \/ |/ / __/
//   / __ / /_/ / / / / /_/ / , __/ /_\ \/ __ |/ / _/ // /_/ /    _\ \  
//  /_/ |_\____/ /_/  \____/_/|_/___/___/_/ |_/_/ /___/\____/_/|_/___/  
//                                                                                                               
//  Seuls autorisés : users connectés et non admin

//est ce que le user est connecté
if (isset($_SESSION["user"])) {
    //si l'utilisateur est un admin il est redirigé vers sa page d'administration
    if ($_SESSION["admin"] == 1) {
        header("location:dashBoardAdmin.php");
    }
} else {
    header("location:login.php");
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Head de la documentation bootstrap -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!-- Boostrap JS -->
    <script src="../js/bootstrap.bundle.min.js"></script>
    <!-- Mon CSS -->
    <link href="../css/monCss.css" rel="stylesheet">
    <!-- Icons -->
    <link href="../css/all.css" rel="stylesheet">
    <title>Dashboard</title>
</head>

<body style='background-color:#181818;color:#FFFFFF'>
    <div class="main">
        <?php include("../php/nav.php"); ?>
        <div class="text-center" style='padding-top:0'>
            <h1>DashBoard</h1>
        </div>
        <h2 class="text-center"><a href="comptes.php" style="color:white">Comptes</a></br><a href="ajoutCompte.php" style='color:#AAAAAA;text-decoration:none'><button type="button" class="btn btn-light" style="background-color:#212121;color:#FFFFFF">Ajouter un compte</button></a></h2>
        <?php
        if (!DisplayAccountsByUser($_SESSION["idUser"])) {
            //si il n'y a aucuns comptes, on redirige l'utilisateur pour qu'il en crée un
            header("location:ajoutCompte.php");
        }
        ?>

        <h2 class="text-center">Mots Clés</h2>
        <div class="container">
            <?php
            //affichage du nuage de mots
            DisplayKeyWordCloud();
            ?>
        </div>
        

        <h2 class="text-center"><a href="budgets.php" style="color:white">Budgets</a></br>
            <a href="ajoutBudget.php" style='color:#AAAAAA;text-decoration:none'><button type="button" class="btn btn-light" style="background-color:#212121;color:#FFFFFF">Ajouter un budget</button></a>
        </h2>
        <?php
        //affichage des différnts budgets
        DisplayAllBudgetsByUser($_SESSION["idUser"])
        ?>
        
        <h2 class="text-center"><a href="operations.php" style="color:white">Operations</a></h2>
        <h2 class="text-center"><a href="ajoutOperation.php" style='color:#AAAAAA;text-decoration:none'><button type="button" class="btn btn-light" style="background-color:#212121;color:#FFFFFF">Ajouter une operation</button></a></h2>

        <?php
        //affichage des derniers opérations 
        DisplayOperationsByUser($_SESSION["idUser"], 5)
        ?>
     

    </div>

    <?php include("../php/footer.php"); ?>

</body>

</html>