<?php
//     __  ___         _            ___       __               
//    /  |/  ___ __ __(___ _ ___   / _ \___  / /  __ _ ___ ____
//   / /|_/ / _ `\ \ / /  ' / -_) / , _/ _ \/ _ \/  ' / -_/ __/
//  /_/  /_/\_,_/_\_/_/_/_/_\__/ /_/|_|\___/_//_/_/_/_\__/_/     
//                                                                                       
//  Nom Du projet : MyBudget
//  Developpeur : Maxime.rhmr
//  Version : 1.0
//  Dernieres modifications : 2021.05.12
//  Maitre enseignant : Mr Garchery Stephane
//  Experts : Mr Terrond Nicolas,Mr Strazzery Mickael
//  Fichier : deleteOperation.php

//prérequis
session_start();
require("../php/fonctions.php");
//prérequis

//     ___  __  ____________  ___  ___________ ______________  _  ______
//    / _ |/ / / /_  __/ __ \/ _ \/  _/ __/ _ /_  __/  _/ __ \/ |/ / __/
//   / __ / /_/ / / / / /_/ / , __/ /_\ \/ __ |/ / _/ // /_/ /    _\ \  
//  /_/ |_\____/ /_/  \____/_/|_/___/___/_/ |_/_/ /___/\____/_/|_/___/  
//                                                                                                               
//  Seuls autorisés : users connectés et non admin

$id = filter_input(INPUT_GET,"id",FILTER_SANITIZE_NUMBER_INT);
//est ce que le user est bien connecté
if (isset($_SESSION["idUser"])) {
    //est ce que le user n'est bien pas admin
    if ($_SESSION["admin"] == 0) {
        if(isUserAllowedOperation($id,$_SESSION["idUser"])){
            //si l'operation as bien été effectuée par le user alors il a le droit de la suprimmer
            DeleteOperation($id);
            header("location:" .$_SERVER['HTTP_REFERER'] ."");
        }else{
            header("location:login.php");
        }
    }else{
        header("location:login.php");
    }
}else{
    header("location:login.php");
}



?>