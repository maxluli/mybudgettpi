<?php
//     __  ___         _            ___       __               
//    /  |/  ___ __ __(___ _ ___   / _ \___  / /  __ _ ___ ____
//   / /|_/ / _ `\ \ / /  ' / -_) / , _/ _ \/ _ \/  ' / -_/ __/
//  /_/  /_/\_,_/_\_/_/_/_/_\__/ /_/|_|\___/_//_/_/_/_\__/_/     
//                                                                                       
//  Nom Du projet : MyBudget
//  Developpeur : Maxime.rhmr
//  Version : 1.0
//  Date de release : 2021.05.20
//  Maitre enseignant : Mr Garchery Stephane
//  Experts : Mr Terrond Nicolas,Mr Strazzery Mickael
//  Fichier : ajoutOperation.php

//prérequis
session_start();
require("../php/fonctions.php");
//prérequis

//     ___  __  ____________  ___  ___________ ______________  _  ______
//    / _ |/ / / /_  __/ __ \/ _ \/  _/ __/ _ /_  __/  _/ __ \/ |/ / __/
//   / __ / /_/ / / / / /_/ / , __/ /_\ \/ __ |/ / _/ // /_/ /    _\ \  
//  /_/ |_\____/ /_/  \____/_/|_/___/___/_/ |_/_/ /___/\____/_/|_/___/  
//                                                                                                               
//  Seuls autorisés : users connectés et non admin

//le user est bien connecté
if (isset($_SESSION["user"])) {
    //le user n'est bien pas un administrateur
    if ($_SESSION["admin"] == 0) {
        //on peut rester sur cette page 
        //la variable message contiendra tout les differents messages d'indication d'erreur 
        $Message = "";
        //set initial des variables 
        $name = "";
        $amount = "";
        $idAccount = "";
        $idBudget = "";
        $Keys = "";
        //on verifie si un formulaire as été envoyé
        if (isset($_POST["submitButton"])) {
            //le formulaire as été envoyé on peut donc traiter les datas
            //recuperation des variables filtrées
            $nameOld = $_POST["name"];
            $name = filter_input(INPUT_POST, "name", FILTER_SANITIZE_STRING);
            $amountOld = $_POST["amount"];
            $amount = filter_input(INPUT_POST, "amount", FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
            $idAccountOld = $_POST["account"];
            $idAccount = filter_input(INPUT_POST, "account", FILTER_SANITIZE_STRING);
            $idBudget = filter_input(INPUT_POST, "budget", FILTER_SANITIZE_NUMBER_INT);
            if(isset($_POST["keys"])){
                $Keys = $_POST["keys"];
            }else{
                $Keys = null;
            }
            

            //on verifie que les deux valeurs ne sont pas vides
            if ($name != "" && $amount != "" && $idAccount != "") {

                if(strlen($name) <= 45){
                    if ($idBudget == "") {
                        $idBudget = null;
                    }
                    if($Keys == null){
                        //je mets a -1 pour que de l'autre coté quand je fais la fonction d'ajout je sache que il n'y a pas de relations a faire
                        $Keys = -1;
                    }
                    if (CreateOperation($name, $amount, $idAccount, $idBudget , $Keys ,$_SESSION["idUser"])) {
                        $Message .= "<div class='alert alert-successful' role='alert'>
                            L'operation as bien été créé
                            </div>";
                            header("location:dashboard.php");
                    } else {
                        $Message .= "<div class='alert alert-danger' role='alert'>
                            L'operation' n'as pas pu être créé
                            </div>";
                    }
                }else{
                    $Message .= "<div class='alert alert-danger' role='alert'>
                    Le nom ne peut pas dépasser 45 caractères
                    </div>";
                }
            } else {
                //la personne n'as mis que des caractères spéciaux ou n'as simplement pas envoyé ce champ
                if($nameOld != "" && $name == "" || $nameOld == ""){
                    $Message .= "<div class='alert alert-danger' role='alert'>
                    Le champ Nom doit être remplis
                    </div>";
                }
                if($amount != "" && $amount == "" || $amountOld == ""){
                    $Message .= "<div class='alert alert-danger' role='alert'>
                    Le champ montant doit être rempli
                    </div>";
                }
                if($idAccountOld != "" && $idAccount == "" || $idAccountOld == ""){
                    $Message .= "<div class='alert alert-danger' role='alert'>
                    Toute opération doit contenir avoir un compte 
                    </div>";
                }
                $Message .= "<div class='alert alert-danger' role='alert'>
                Tout les champs du formulaire doivent être remplis
                </div>";
            }
        }
    } else {
        header("location:login.php");
    }
} else {
    header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Head de la documentation bootstrap -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!-- Boostrap JS -->
    <script src="../js/bootstrap.bundle.min.js"></script>
    <!-- Mon CSS -->
    <link href="../css/monCss.css" rel="stylesheet">
    <!-- Icons -->
    <link href="../css/all.css" rel="stylesheet">
    <title>Ajout Operation</title>
</head>

<body style='background-color:#181818;color:#FFFFFF'>
    <div class="main">
        <?php include("../php/nav.php"); ?>
        <div class="text-center" style='padding-top:4%'>
            <h1>Ajouter une operation</h1>
        </div>

        <div class='container rounded' style='background-color:#212121;margin-top:4%;margin-bottom:4%;padding-left:8%;padding-right:8%;padding-top:5%;padding-bottom:2.5%;width:40%'>
            <h1>Nouvelle operation</h1>

            <form action="#" method="POST">
                <label for="email" class="form-label">Nom</label>
                <input type='text' name='name' required='true' placeholder="" class="form-control" value="<?= $name ?>"></b>
                <div id="HelpBlock" class="form-text">
                    Exemple "courses" max 45 caractères
                </div>

                <label for="email" class="form-label">Montant</label>
                <input type='number' name='amount' required='true' placeholder="" class="form-control" value="<?= $amount ?>"></b>
                <div id="HelpBlock" class="form-text">
                    Pour montrer une depense mettre - ex(-350)
                </div>
                <label for="account" class="form-label">Compte</label>
                <select class="form-select form-control" aria-label="Choisir un compte" name="account">
                    <option selected></option>
                    <?php
                    $accounts = GetAllAccountsByUser($_SESSION["idUser"]);
                    foreach ($accounts as $a) {
                        if($idAccount == $a["id"]){
                            echo '<option value="' . $a["id"] . '" selected>' . $a["libele"] . '</option>';
                        }else{
                            echo '<option value="' . $a["id"] . '" >' . $a["libele"] . '</option>';
                        }
                        
                    }
                    ?>

                </select>
                <label for="budget" class="form-label">Budget</label>
                <select class="form-select form-control" aria-label="Choisir un compte" name="budget">
                    <option selected></option>
                    <?php
                    $budgets = GetAllBudgetsByUser($_SESSION["idUser"]);
                    foreach ($budgets as $b) {
                        if($idBudget == $b["id"]){
                            echo '<option value="' . $b["id"] . '" selected>' . $b["titre"] . '</option>';
                        }else{
                            echo '<option value="' . $b["id"] . '" >' . $b["titre"] . '</option>';
                        }                     
                    }
                    ?>

                </select>
                <label for="budget" class="form-label">Mots clefs</label>
                <select class="form-select form-control" aria-label="Choisir un compte" name="keys[]" multiple>
                    <option></option>
                    <?php
                    $keyWords = GetKeyWords($_SESSION["idUser"]);
                    foreach ($keyWords as $a) {
                        $found = 0;
                        foreach($Keys as $k){
                            if($k["id"] == $a["id"]){
                                $found = 1;
                            }
                        }
                       if($found == 1){
                        echo '<option value="' . $a["id"] . '" selected>' . $a["libele"] . '</option>'; 
                       }else{
                        echo '<option value="' . $a["id"] . '" >' . $a["libele"] . '</option>'; 
                       }
                    }
                    ?>

                </select>
                
                <div class="text-center" style='margin-top:5%;'>
                    <button type="submit" name="submitButton" class="btn btn-secondary btn-lg form-control" style="width:50%; min-width:80px">Creer</button></br>
                </div>
            </form>
            <?php
            if ($Message != "") {
                echo "<div class='text-center' style='margin-top:5%'>
                $Message
            </div>";
            }
            ?>
        </div>
    </div>
    <?php include("../php/footer.php"); ?>
</body>
</html>