<?php
//     __  ___         _            ___       __               
//    /  |/  ___ __ __(___ _ ___   / _ \___  / /  __ _ ___ ____
//   / /|_/ / _ `\ \ / /  ' / -_) / , _/ _ \/ _ \/  ' / -_/ __/
//  /_/  /_/\_,_/_\_/_/_/_/_\__/ /_/|_|\___/_//_/_/_/_\__/_/     
//                                                                                       
//  Nom Du projet : MyBudget
//  Developpeur : Maxime.rhmr
//  Version : 1.0
//  Date de release : 2021.05.20
//  Maitre enseignant : Mr Garchery Stephane
//  Experts : Mr Terrond Nicolas,Mr Strazzery Mickael
//  Fichier : deleteBudget.php

//prérequis
session_start();
require("../php/fonctions.php");
//prérequis

//     ___  __  ____________  ___  ___________ ______________  _  ______
//    / _ |/ / / /_  __/ __ \/ _ \/  _/ __/ _ /_  __/  _/ __ \/ |/ / __/
//   / __ / /_/ / / / / /_/ / , __/ /_\ \/ __ |/ / _/ // /_/ /    _\ \  
//  /_/ |_\____/ /_/  \____/_/|_/___/___/_/ |_/_/ /___/\____/_/|_/___/  
//                                                                                                               
//  Seuls autorisés : users connectés et non admin
$id = filter_input(INPUT_GET,"id",FILTER_SANITIZE_NUMBER_INT);
//est ce que le user est connecté
if (isset($_SESSION["user"])) {
    //est ce que le user n'est bien pas un admin
    if ($_SESSION["admin"] == 0) {
        //si le user as bien le droit , il peut suprimmer le budget
        if(isAllowedBudget($_SESSION["idUser"],$id)){
            DeleteBudget($id);
            header("location:dashBoard.php");
        }else{
            header("location:login.php");
        }
    }else{
        header("location:login.php");
    }
}else{
    header("location:login.php");
}



?>