<?php
//     __  ___         _            ___       __               
//    /  |/  ___ __ __(___ _ ___   / _ \___  / /  __ _ ___ ____
//   / /|_/ / _ `\ \ / /  ' / -_) / , _/ _ \/ _ \/  ' / -_/ __/
//  /_/  /_/\_,_/_\_/_/_/_/_\__/ /_/|_|\___/_//_/_/_/_\__/_/     
//                                                                                       
//  Nom Du projet : MyBudget
//  Developpeur : Maxime.rhmr
//  Version : 1.0
//  Date de release : 2021.05.20
//  Maitre enseignant : Mr Garchery Stephane
//  Experts : Mr Terrond Nicolas,Mr Strazzery Mickael
//  Fichier : ajoutBudget.php

//prérequis
session_start();
require("../php/fonctions.php");
//prérequis

//     ___  __  ____________  ___  ___________ ______________  _  ______
//    / _ |/ / / /_  __/ __ \/ _ \/  _/ __/ _ /_  __/  _/ __ \/ |/ / __/
//   / __ / /_/ / / / / /_/ / , __/ /_\ \/ __ |/ / _/ // /_/ /    _\ \  
//  /_/ |_\____/ /_/  \____/_/|_/___/___/_/ |_/_/ /___/\____/_/|_/___/  
//                                                                                                               
//  Seuls autorisés : users connectés et non admin

//est ce que le user est connecté 
if (isset($_SESSION["user"])) {
    //est ce que le user est bien un utilisateur normal et non un admin
    if ($_SESSION["admin"] == 0) {
        //la variable message contiendra tout les differents messages d'indication d'erreur 
        $Message = "";
        $name = "";
        $amount = "";
        //on verifie si un formulaire as été envoyé
        if (isset($_POST["submitButton"])) {
            //recuperation des variables filtrées
            $name = filter_input(INPUT_POST, "name", FILTER_SANITIZE_STRING);
            $amount = filter_input(INPUT_POST, "amount", FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
            //on verifie que les deux valeurs ne sont pas vides
            if ($name != "" && $amount != "") {
                if (strlen($name) <= 45) {
                    if (CreateBudget($name, $amount, $_SESSION["idUser"])) {
                        //si la création ne pose pas de soucis alors on peut informer l'utilisateur et le rediriger
                        $Message .= "<div class='alert alert-successful' role='alert'>
                        Le Budget as bien été crée
                        </div>";
                        header("location:dashBoard.php");
                    } else {
                        //si il y a eu une erreur dans le traitement de la requette sql
                        $Message .= "<div class='alert alert-danger' role='alert'>
                        Le budget n'as pas pu être créer
                        </div>";
                    }
                } else {
                    $Message .= "<div class='alert alert-danger' role='alert'>
                        Le nom ne peut pas faire plus de 45 caractères
                        </div>";
                }
            } else {
                //si tous les champs m
                $Message .= "<div class='alert alert-danger' role='alert'>
                Tout les champs du formulaire doivent être remplis
                </div>";
            }
        }
    } else {
        header("location:login.php");
    }
} else {
    header("location:login.php");
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Head de la documentation bootstrap -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!-- Boostrap JS -->
    <script src="../js/bootstrap.bundle.min.js"></script>
    <!-- Mon CSS -->
    <link href="../css/monCss.css" rel="stylesheet">
    <!-- Icons -->
    <link href="../css/all.css" rel="stylesheet">
    <title>Ajout Budget</title>
</head>

<body style='background-color:#181818;color:#FFFFFF'>
    <div class="main">
        <!--    nav  -->
        <?php include("../php/nav.php"); ?>
        <div class="text-center" style='padding-top:3%'>
            <h1>Ajouter un budget</h1>
        </div>

        <div class='container rounded' style='background-color:#212121;margin-top:4%;padding-left:8%;padding-right:8%;padding-top:2.5%;padding-bottom:2.5%;width:40%'>
            <h1>Nouveau Budget</h1>
            <form action="#" method="POST">
                <label for="email" class="form-label">Nom</label>
                <input type='text' name='name' required='true' placeholder="" class="form-control" value="<?= $name ?>"></b>
                <div id="HelpBlock" class="form-text">
                    Exemple "Nourriture" max 45 caractères
                </div>

                <label for="email" class="form-label">Montant Mensuel</label>
                <input type='number' name='amount' required='true' placeholder="" class="form-control" value="<?= $amount ?>"></b>
                <div id="HelpBlock" class="form-text">
                    Exemple "300"
                </div>
                <div class="text-center" style='margin-top:5%;'>
                    <button type="submit" name="submitButton" class="btn btn-secondary btn-lg form-control" style="width:50%; min-width:80px">Creer</button></br>
                </div>
            </form>
            <?php
            if ($Message != "") {
                echo "<div class='text-center' style='margin-top:5%'>
                $Message
            </div>";
            }
            ?>
        </div>
    </div>
    <!--    footer  -->
    <?php include("../php/footer.php"); ?>
</body>

</html>