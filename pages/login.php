<?php
//     __  ___         _            ___       __               
//    /  |/  ___ __ __(___ _ ___   / _ \___  / /  __ _ ___ ____
//   / /|_/ / _ `\ \ / /  ' / -_) / , _/ _ \/ _ \/  ' / -_/ __/
//  /_/  /_/\_,_/_\_/_/_/_/_\__/ /_/|_|\___/_//_/_/_/_\__/_/     
//                                                                                       
//  Nom Du projet : MyBudget
//  Developpeur : Maxime.rhmr
//  Version : 1.0
//  Date de release : 2021.05.20
//  Maitre enseignant : Mr Garchery Stephane
//  Experts : Mr Terrond Nicolas,Mr Strazzery Mickael
//  Fichier : login.php

//prérequis
session_start();
require("../php/fonctions.php");
//prérequis

//     ___  __  ____________  ___  ___________ ______________  _  ______
//    / _ |/ / / /_  __/ __ \/ _ \/  _/ __/ _ /_  __/  _/ __ \/ |/ / __/
//   / __ / /_/ / / / / /_/ / , __/ /_\ \/ __ |/ / _/ // /_/ /    _\ \  
//  /_/ |_\____/ /_/  \____/_/|_/___/___/_/ |_/_/ /___/\____/_/|_/___/  
//                                                                                                               
//  Seuls autorisés : tout le monde

if (isset($_SESSION["user"])) {
    if ($_SESSION["admin"] == 1) {
        header("location:dashBoardAdmin.php");
    } else {
        header("location:dashBoard.php");
    }
}

//la variable message contiendra tout les differents messages d'indication d'erreur 
$Message = "";
//set initial des variables 
$email = "";
$password = "";
//on verifie si un formulaire as été envoyé
if (isset($_POST["submitButton"])) {
    //le formulaire as été envoyé on peut donc traiter les datas

    //recuperation des variables filtrées
    $email = filter_input(INPUT_POST, "email", FILTER_SANITIZE_EMAIL);
    $password = filter_input(INPUT_POST, "pass", FILTER_SANITIZE_STRING);
    //on verifie que les deux valeurs ne sont pas vides
    if ($email != "" && $password != "") {
        //on as deux valeurs non nulles et filtrées , maintenant on peut les comparer avec les valeurs en base de donnée
        $isAdmin = CompareLogin($email, $password);
        //var_dump(CompareLogin($email, $password));
        if ($isAdmin[2] == 0) {
            //si l'identification rate
            $Message .= "<div class='alert alert-danger' role='alert'>
            Les identifiants sont incorrects , si vous n'avez pas de compte <a href='register.php' class='alert-link'>inscrivez vous ici</a>
            </div>";
        } else {
            $Message .= "<div class='alert alert-success' role='alert'>
            Connexion réussie
            </div>";
            $_SESSION["user"] = $email;
            $_SESSION["admin"] = $isAdmin[0];
            $_SESSION["idUser"] = $isAdmin[1];
            header("location:login.php");
        }
    } else {
        //si un peit malin tente de ne pas envoyer tous les champs
        $Message .= "<div class='alert alert-danger' role='alert'>
        Tout les champs du formulaire doivent être remplis
        </div>";
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Head de la documentation bootstrap -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!-- Boostrap JS -->
    <script src="../js/bootstrap.bundle.min.js"></script>
    <!-- Mon CSS -->
    <link href="../css/monCss.css" rel="stylesheet">
    <!-- Icons -->
    <link href="../css/all.css" rel="stylesheet">
    <title>Login</title>
</head>

<body style='background-color:#181818;color:#FFFFFF'>
    <div class="main">
        <?php include("../php/nav.php"); ?>
        <div class="text-center" style='padding-top:3%'>
            <h1>Se connecter</h1>
        </div>

        <div class='container rounded' style='background-color:#212121;margin-top:4%;padding-left:8%;padding-right:8%;padding-top:2.5%;padding-bottom:2.5%;width:40%'>
            <h1>Login</h1>
            <form action="#" method="POST">
                <label for="email" class="form-label">Email</label>
                <input type='email' name='email' required='true' placeholder="" class="form-control" value="<?= $email ?>"></b>
                <div id="emailHelpBlock" class="form-text">
                    Exemple Test@gmail.com
                </div>
                <label for="pass" class="form-label">Password</label>
                <input type="password" required="true" name="pass" placeholder="" class="form-control">
                <div id="passwordHelpBlock" class="form-text">
                    Exemple Banane234#
                </div>
                <div class="text-center" style='margin-top:5%;'>
                    <button type="submit" name="submitButton" class="btn btn-secondary btn-lg form-control" style="width:50%; min-width:80px" data-bs-toggle="tooltip" data-bs-placement="top" title="Se connecter">Login</button></br>
                    Vous n'avez pas de compte ? <a href="register.php" style="text-decoration:none;color:#AAAAAA"> S'enregistrer</a>
                </div>
            </form>
            <?php
            if ($Message != "") {
                echo "<div class='text-center' style='margin-top:5%'>
                $Message
            </div>";
            }
            ?>
        </div>
    </div>
    <?php include("../php/footer.php"); ?>
</body>

</html>