-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 10, 2021 at 12:22 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mybudget`
--

-- --------------------------------------------------------

--
-- Table structure for table `budgets`
--

CREATE TABLE `budgets` (
  `id` int(11) UNSIGNED NOT NULL,
  `titre` varchar(45) NOT NULL,
  `montant_mensuel` decimal(10,2) NOT NULL,
  `utilisateurs_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `budgets`
--

INSERT INTO `budgets` (`id`, `titre`, `montant_mensuel`, `utilisateurs_id`) VALUES
(6, 'Courses', '500.00', 4),
(7, 'Voiture', '800.00', 4),
(8, 'Cadeaux', '200.00', 4),
(13, 'asdgjkl', '123589.00', 8),
(14, 'Divers', '800.00', 4);

-- --------------------------------------------------------

--
-- Table structure for table `comptes`
--

CREATE TABLE `comptes` (
  `id` int(11) UNSIGNED NOT NULL,
  `libele` varchar(45) NOT NULL,
  `numero` varchar(45) NOT NULL,
  `banque` varchar(45) NOT NULL,
  `utilisateurs_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comptes`
--

INSERT INTO `comptes` (`id`, `libele`, `numero`, `banque`, `utilisateurs_id`) VALUES
(6, 'LESMOMES', '1564654512', 'Credit suisse', 7),
(16, 'Compte Famille', '545656456', 'Raifeisen', 4),
(20, 'priv', '32165332', 'gfdsaéoiufedsaélj', 8),
(21, 'Compte privé', '11651856132', 'Credit suisse', 4),
(22, 'Epargne', '21561', 'reiffeisne', 4),
(25, 'test', '14456156', 'Credit suisse', 9);

-- --------------------------------------------------------

--
-- Table structure for table `motsclefs`
--

CREATE TABLE `motsclefs` (
  `id` int(11) UNSIGNED NOT NULL,
  `libele` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `motsclefs`
--

INSERT INTO `motsclefs` (`id`, `libele`) VALUES
(1, 'Papa'),
(2, 'maman'),
(3, 'Lucie'),
(6, 'Teest');

-- --------------------------------------------------------

--
-- Table structure for table `operationhasmotsclef`
--

CREATE TABLE `operationhasmotsclef` (
  `operations_id` int(11) UNSIGNED NOT NULL,
  `motsClefs_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `operationhasmotsclef`
--

INSERT INTO `operationhasmotsclef` (`operations_id`, `motsClefs_id`) VALUES
(49, 3),
(74, 2),
(75, 3),
(75, 6),
(78, 1),
(79, 3),
(82, 3),
(89, 2),
(90, 2);

-- --------------------------------------------------------

--
-- Table structure for table `operations`
--

CREATE TABLE `operations` (
  `id` int(11) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `libele` varchar(150) NOT NULL,
  `montant` decimal(10,2) NOT NULL,
  `comptes_id` int(11) UNSIGNED NOT NULL,
  `budget_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `operations`
--

INSERT INTO `operations` (`id`, `date`, `libele`, `montant`, `comptes_id`, `budget_id`) VALUES
(17, '2021-05-10', 'Jointdeculasse', '-500.00', 1, 1),
(18, '2021-05-10', 'Ventetwingo', '1000.00', 1, 1),
(20, '2021-05-10', 'TransmissiondelaBM', '-2000.00', 5, 1),
(21, '2021-05-10', 'salaire', '3000.00', 13, 0),
(23, '2021-05-10', 'Impots', '-800.00', 13, 0),
(24, '2021-05-10', 'Filldedebutdemois', '8000.00', 14, 0),
(26, '2021-05-10', 'Debutdemois', '500.00', 16, 0),
(27, '2021-05-10', 'CoursesSamedimatin', '-55.00', 13, 6),
(28, '2021-05-10', 'EmbrayageFoutu', '-990.00', 14, 7),
(29, '2021-05-10', 'Collier', '300.00', 17, 10),
(30, '2021-05-10', 'Collier', '-300.00', 17, 0),
(31, '2021-05-10', 'asd', '820.00', 17, 12),
(32, '2021-05-10', 'asd', '-8900.00', 17, 12),
(33, '2021-05-10', 'dASD', '600.00', 17, 0),
(34, '2021-05-10', 'kjhlahsd', '12655684.00', 20, 13),
(36, '2021-05-10', 'Collier', '-300.00', 16, 8),
(39, '2021-05-10', 'Canap', '400.00', 16, 9),
(40, '2021-05-10', 'Canap', '-300.00', 16, 0),
(41, '2021-05-10', 'Nouveau drone', '-80000.00', 16, 0),
(42, '2021-05-10', 'Salaire', '9500.00', 21, 0),
(44, '2021-05-10', 'Findemois', '-300.00', 16, 0),
(46, '2021-05-10', 'Urgence', '-250.00', 22, 0),
(49, '2021-05-10', 'vente drone', '15000.00', 16, 14),
(50, '2021-05-10', 'drone', '-400.00', 22, 14),
(51, '2021-05-10', 'Revente Vêtements', '20000.00', 22, 8),
(74, '2021-05-10', 'Achat Veste', '-60000.00', 21, 8),
(75, '2021-04-10', 'Achat Canapé', '-60000.00', 16, 6),
(79, '2021-05-10', 'Vente Parfum', '50.00', 21, 7);

-- --------------------------------------------------------

--
-- Table structure for table `utilisateurs`
--

CREATE TABLE `utilisateurs` (
  `id` int(11) UNSIGNED NOT NULL,
  `login` varchar(45) NOT NULL,
  `password` varchar(256) NOT NULL,
  `est_administrateur` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `utilisateurs`
--

INSERT INTO `utilisateurs` (`id`, `login`, `password`, `est_administrateur`) VALUES
(3, 'Admin@gmail.com', '$2y$10$qWN13wglbQ5j.QgM27Wea.piZFSZbsW1NzxSq7MJXIEPNwLv2akLS', 1),
(4, 'maxluligames@gmail.com', '$2y$10$XZN2n7u6zM5U2CivEJt.aObv00fax2rKvMUGLmAdupQdkfTiAWfAe', 0),
(5, 'mussletesteur@haha.com', '$2y$10$lVdgD1R7t11fQ06ube1x/eI1nGTgXRnAfSCcKjc7geaVGtxUTNPsK', 0),
(6, 'lol@lol.ch', '$2y$10$L23lE1kSax0MGdVxP1L4wu3JHEMf1O.P9QH0kXwdGejdFXF/Ed5jS', 0),
(7, 'Super@gmail.com', '$2y$10$tOTh1GVmdC0tUxiR1xln9O46yZwhv/yJmlpuODU5DXc3vcgZV71YK', 0),
(8, 'brian.gl@eduge.ch', '$2y$10$OljAYSdRAoXVbtf9mi9zxu5ypbGGraYZnZyp1OySUQ7WI9ZqqZJpW', 0),
(9, 'Test@gmail.com', '$2y$10$h4QMAWD/pEr8VZCc2TXuM.iGK9fEdGxjy9x/D4mMI10mZFMRLKfda', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `budgets`
--
ALTER TABLE `budgets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `utilisateurs_id` (`utilisateurs_id`);

--
-- Indexes for table `comptes`
--
ALTER TABLE `comptes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `utilisateurs_id` (`utilisateurs_id`);

--
-- Indexes for table `motsclefs`
--
ALTER TABLE `motsclefs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `operationhasmotsclef`
--
ALTER TABLE `operationhasmotsclef`
  ADD PRIMARY KEY (`operations_id`,`motsClefs_id`);

--
-- Indexes for table `operations`
--
ALTER TABLE `operations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comptes_id` (`comptes_id`),
  ADD KEY `budget_id` (`budget_id`);

--
-- Indexes for table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `budgets`
--
ALTER TABLE `budgets`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `comptes`
--
ALTER TABLE `comptes`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `motsclefs`
--
ALTER TABLE `motsclefs`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `operations`
--
ALTER TABLE `operations`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
