<nav class="navbar navbar-expand-lg " style='background-color:#212121;color:#FFFFFF'>
    <div class="container-fluid">
        <a class="" href="login.php" style='color:#FFFFFF;text-decoration:none'>
            <h1>MyBudget</h1>
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fas fa-bars" style="color:white"></i>
        </button>
        <?php
        //
        if (isset($_SESSION["user"])) {
            if ($_SESSION["user"] != "") {
                if ($_SESSION["admin"] == 1) {
                    //navigation pour les admins connectés
                    echo '
                    <div class="collapse navbar-collapse" id="navbarNavDropdown" >
                    <ul class="navbar-nav">
                    <li class="nav-item">
                    <a class="nav-link" href="disconnect.php" style="color:Red;text-decoration:none">Deconnexion</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="faq.php" style="color:#2F80ED;text-decoration:none">FAQ</a>
                    </li>
                    </ul>                  
                    ';
                } else {
                    //navigation pour les users connectés
                    echo '
                <div class="collapse navbar-collapse" id="navbarNavDropdown" >
                <ul class="navbar-nav">
                <li class="nav-item">
                <a class="nav-link" href="comptes.php" style="color:#FFFFFF;text-decoration:none">Comptes</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="budgets.php" style="color:#FFFFFF;text-decoration:none">Budgets</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="operations.php" style="color:#FFFFFF;text-decoration:none">Opérations</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="disconnect.php" style="color:Red;text-decoration:none">Deconnexion</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="faq.php" style="color:#2F80ED;text-decoration:none">FAQ</a>
                </li>
                </ul>     
                ';
                }
            }
        } else {
            //navigation pour les visiteurs
            echo '
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
            <li class="nav-item">
            <a class="nav-link" href="faq.php" style="color:#2F80ED;text-decoration:none">FAQ</a>
            </li>
            </ul>      
            </div>
            ';
        }
        ?>

    </div>
</nav>