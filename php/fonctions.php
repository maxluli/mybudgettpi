<?php
//     __  ___         _            ___       __               
//    /  |/  ___ __ __(___ _ ___   / _ \___  / /  __ _ ___ ____
//   / /|_/ / _ `\ \ / /  ' / -_) / , _/ _ \/ _ \/  ' / -_/ __/
//  /_/  /_/\_,_/_\_/_/_/_/_\__/ /_/|_|\___/_//_/_/_/_\__/_/     
//                                                                                       
//  Nom Du projet : MyBudget
//  Developpeur : Maxime.rhmr
//  Version : 1.0
//  Date de release : 2021.05.20
//  Maitre enseignant : Mr Garchery Stephane
//  Experts : Mr Terrond Nicolas,Mr Strazzery Mickael
//  Fichier : fonctions.php

//Prerequis
require("../sql/INC.php");

//     __________   _______________
//    / __/ __/ /  / __/ ___/_  __/
//   _\ \/ _// /__/ _// /__  / /   
//  /___/___/____/___/\___/ /_/    
//                             
//  Fonctions qui effectuent des opérations de selection dans la base de donnée

/** Fonction qui va récupèrer toutes les informations du user selon son email/login
 * @param string $email email de l'utilisateur
 * @return Array ["id"],["login"],["password"],["est_administrateur"]
 */
function GetUserByEmail($email)
{
    connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
    $requette = connexion()->prepare("SELECT * FROM utilisateurs WHERE login = '$email'");
    $requette->execute();
    $user = $requette->fetchAll();
    return $user;
}
/** Fonction qui va vérfier que le budget appartiens bien au user donné
 * @param int $idUser id du user en question
 * @param int $idBudget id du budget en question
 * @return bool true = le buget appartiens bien au user/false le budget ne lui appartiens pas
 */
function CheckBudgetByUser($idUser, $idBudget)
{
    connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
    $requette = connexion()->prepare("SELECT * FROM budgets WHERE utilisateurs_id = $idUser AND id = $idBudget");
    $requette->execute();
    $user = $requette->fetchAll();
    if ($user != null) {
        return true;
    } else {
        return false;
    }
}
/** Fonction qui va vérfier que le compte appartiens bien au user donné
 * @param int $idUser id du user en question
 * @param int $idAccount id du compte en question
 * @return bool true = le compte appartiens bien au user/false le compte ne lui appartiens pas
 */
function CheckAccountByUser($idUser, $idAccount)
{
    connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
    $requette = connexion()->prepare("SELECT * FROM comptes WHERE utilisateurs_id = $idUser AND id = $idAccount");
    $requette->execute();
    $user = $requette->fetchAll();
    if ($user != null) {
        return true;
    } else {
        return false;
    }
}

/** Fonction qui va récupèrer la somme des operations sur le budget en question le tout sur les derniers mois et sous forme de tableau pour voir quel mois représente quelle somme
 * @param int $monthToBack nombre de mois à prendre en compte $idBudget 
 * @param int $idBudget id du budget en question  
 * @return Array SUM["montant"],MONTH["DATE"] 
 */
function GetOperationsFromBudgetWithMonth($monthToBack, $idBudget)
{
    //creation de la date jusqu'a laquelle on vas aller chercher les infos
    $theDate = date("Y.m.d", strtotime("-" . $monthToBack . " months"));
    connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
    $requette = connexion()->prepare("SELECT SUM(montant),MONTH(date) FROM operations WHERE budget_id = $idBudget AND date >= '$theDate' GROUP BY MONTH(date) DESC");
    $requette->execute();
    $operations = $requette->fetchAll();
    return $operations;
}
/** Fonction qui va récupèrer la liste des operations selon l'id fournis
 * @param int $idOperation id de l'operation en question 
 * @return Array ["id"],["date"],["libele"],["montant"],[comptes_id],[budget_id]
 */
function GetOperationById($idOperation)
{
    connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
    $requette = connexion()->prepare("SELECT * FROM operations WHERE id = '$idOperation'");
    $requette->execute();
    $operation = $requette->fetchAll();
    return $operation;
}
/** Fonction qui va récupèrer toutes les infos de tous les comptes d'un user donné
 * @param int $idUser id du user en question
 * @return Array ["id"],["login"],["pasword"],["est_administrateur"]
 */
function GetAllAccountsByUser($idUser)
{
    connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
    $requette = connexion()->prepare("SELECT * FROM comptes WHERE utilisateurs_id = '$idUser'");
    $requette->execute();
    $comptes = $requette->fetchAll();
    return $comptes;
}
/** Fonction qui va toutes les infos des operations selon le user donné
 * @param int $idUser id du user en question
 * @param int $limit nombre d'operation désiré
 * @return Array ["id"],["date"],["libele"],["montant"],[comptes_id],[budget_id]
 */
function GetAllOperationsByUser($idUser, $limit)
{
    //recuperation 
    connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
    $requette = connexion()->prepare("SELECT id FROM comptes WHERE utilisateurs_id = $idUser");
    $requette->execute();
    $comptes = $requette->fetchAll();
    //Recuperation des ids de comptes appartenants a un user
    $ids = "";
    $q = 0;
    foreach ($comptes as $c) {
        $ids .= $c["id"] . ",";
        $q++;
    }
    //Convertion des id en une chaine de caractères a pouvoir mettre dans une requette ex(12,14,25) sans oublier de suprimemr le dernier ','
    $ids = substr($ids, 0, -1);
    connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
    $requette = connexion()->prepare("SELECT * FROM operations WHERE comptes_id IN ($ids) ORDER BY id DESC LIMIT $limit");
    $requette->execute();
    $operations = $requette->fetchAll();
    return $operations;
}
/** Fonction qui va recupèrer toutes les infos des budgets d'un user donné
 * @param int $idUser id du user en question
 * @return Array ["id"],["titre"],["montant_mensuel"],["utilisateurs_id"]
 */
function GetAllBudgetsByUser($idUser)
{
    connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
    $requette = connexion()->prepare("SELECT * FROM budgets WHERE utilisateurs_id = '$idUser'");
    $requette->execute();
    $comptes = $requette->fetchAll();
    return $comptes;
}
/** Fonction qui va récupèrer toutes les informations des operations qui sont liées au budget donné
 * @param int $idBudget id du budget en question
 * @return Array ["id"],["date"],["libele"],["montant"],[comptes_id],[budget_id]
 */
function GetAllOperationsAsBudget($idBudget)
{
    connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
    $requette = connexion()->prepare("SELECT * FROM operations WHERE budget_id = '$idBudget'");
    $requette->execute();
    $operations = $requette->fetchAll();
    return $operations;
}
/** Fonction qui va récupèrer tous les mots clés
 * @return Array ["id"],["libele"]
 */
function GetKeyWords()
{
    connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
    $requette = connexion()->prepare("SELECT * FROM motsclefs ORDER BY libele ASC");
    $requette->execute();
    $operations = $requette->fetchAll();
    return $operations;
}
/** Fonction qui va récupèrer toutes les infos d'un mot clé donnée
 * @param int $idKey id du mot clé en question
 * @return Array ["id"],["libele"]
 */
function GetKeyWordById($idKey)
{
    connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
    $requette = connexion()->prepare("SELECT * FROM motsclefs WHERE id = " . $idKey);
    $requette->execute();
    $operations = $requette->fetchAll();
    return $operations;
}
/** Fonction qui va récupèrer toutes les informations des operations d'un budget ce mois ci
 * @param int $idBudget id du budget
 * @return Array ["id"],["date"],["libele"],["montant"],[comptes_id],[budget_id]
 */
function getAllOperationsASBudgetThisMonth($idBudget)
{
    connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
    $requette = connexion()->prepare("SELECT * FROM operations WHERE budget_id = '$idBudget' AND MONTH(date) = MONTH(CURRENT_DATE()) AND YEAR(date) = YEAR(CURRENT_DATE())");
    $requette->execute();
    $operations = $requette->fetchAll();
    return $operations;
}
/** Fonction qui va récupèrer la somme des operations d'un budget ce mois ci
 * @param int $idBudget id du budget
 * @return Array ["SUM(montant)"]
 */
function GetSommeOnBudget($idBudget)
{
    connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
    $requette = connexion()->prepare("SELECT SUM(montant) FROM operations WHERE budget_id = '$idBudget' AND MONTH(date) = MONTH(CURRENT_DATE()) AND YEAR(date) = YEAR(CURRENT_DATE()) ");
    $requette->execute();
    $somme = $requette->fetchAll();
    return $somme;
}
/** Fonction qui va récupèrer la somme des operations d'un compte
 * @param int $idAccount id du compte en question
 * @return Array ["SUM(montant)"]
 */
function GetSommeOnAccount($idAccount)
{
    connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
    $requette = connexion()->prepare("SELECT SUM(montant) FROM operations WHERE comptes_id = '$idAccount' ");
    $requette->execute();
    $somme = $requette->fetchAll();
    return $somme;
}
/** Fonction qui va récupèrer toutes les infos d'un compte selon l'utilisateur pour ensuite verifier si le compte appartiens a l'utilisateur donné
 * @param int $idAccount id du compte en question
 * @param int $idUser id du compte en question
 * @return Array ["id"]["libele"]["numero"]["banque"]["utilisateurs_id"]
 */
function GetIfAccountAnduserMatch($idAccount, $idUser)
{
    connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
    $requette = connexion()->prepare("SELECT * FROM comptes WHERE utilisateurs_id = $idUser AND id = $idAccount");
    $requette->execute();
    $account = $requette->fetchAll();
    return $account;
}
/** Fonction qui va récupèrer toutes les infos d'un budget selon l'utilisateur pour ensuite verifier si le budget appartiens a l'utilisateur donné
 * @param int $idBudget id du Budget en question
 * @param int $idUser id du compte en question
 * @return Array ["id"],["titre"],["montant_mensuel"],["utilisateurs_id"]
 */
function GetIfBudgetAnduserMatch($idBudget, $idUser)
{
    connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
    $requette = connexion()->prepare("SELECT * FROM budgets WHERE utilisateurs_id = $idUser AND id = $idBudget");
    $requette->execute();
    $budget = $requette->fetchAll();
    return $budget;
}
/** Fonction qui va récupèrer toutes les operations liées a un compte
 * @param int $idAccount id du compte en question
 * @return Array ["id"],["date"],["libele"],["montant"],[comptes_id],[budget_id]
 */
function getAllOperationsAsAccount($idAccount)
{
    connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
    $requette = connexion()->prepare("SELECT * FROM operations WHERE comptes_id = '$idAccount'");
    $requette->execute();
    $operations = $requette->fetchAll();
    return $operations;
}
/** Fonction qui va récupèrer l'id du user qui as créé l'operation donnée
 * @param int $idoperation id de l'operation en question
 * @return Array ["id"] OU null si l'operation n'as pas réussi
 */
function GetIdUserByOperation($idOperation)
{
    connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
    $requette = connexion()->prepare("SELECT comptes_id FROM operations WHERE id = '$idOperation'");
    $requette->execute();
    $accountId = $requette->fetchAll();
    //recuperation de l'id du compte auquel est relié l'operation
    if ($accountId != null) {
        $idCompte = $accountId[0]["comptes_id"];
        connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
        $requette = connexion()->prepare("SELECT utilisateurs_id FROM comptes WHERE id = '$idCompte'");
        $requette->execute();
        $userId = $requette->fetchAll();
        //recuperation de l'id du user a qui appartiens le compte en question
        if ($userId != null) {
            return $userId[0]["utilisateurs_id"];
        } else {
            //si aucun user n'est trouvé
            return null;
        }
    } else {
        //si aucun compte n'est trouvé
        return null;
    }
}
/** Fonction qui va récupèrer le nombre d'iteration de chaque mot clé
 * @return Array ["occ"]["motsClefs_id"]
 */
function GetAllKeyWordIterations()
{
    connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
    //on récupère le nombre de fois qu'apparait un mot clé en les regroupant par mot clé identiques
    $requette = connexion()->prepare("SELECT COUNT(`motsClefs_id`) as occ,`motsClefs_id` FROM operationhasmotsclef GROUP BY `motsClefs_id`");
    $requette->execute();
    $KeyWords = $requette->fetchAll();
    return $KeyWords;
}
/** Fonction qui va récupèrer les mots clés d'une opération
 * @param int $idOperation id de l'operation en question
 * @return Array ["id"]["libele"]
 */
function GetKeyWordByOperation($idOperation)
{
    connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
    $requette = connexion()->prepare("SELECT * FROM motsclefs as K,operationhasmotsclef as OK WHERE k.id = OK.motsClefs_id AND OK.operations_id = " . $idOperation);
    $requette->execute();
    //var_dump($requette);
    $KeyWord = $requette->fetchAll();
    return $KeyWord;
}

//     ___  ______   ______________
//    / _ \/ __/ /  / __/_  __/ __/
//   / // / _// /__/ _/  / / / _/  
//  /____/___/____/___/ /_/ /___/  
//    
//  Fonctions qui effectuent des requêtes de suppression sur la base de donnée

/** Fonction qui suprimme un mot clé et ses conséquences avec un id donné
 * @param int $idKeyWord id du mot clé en question
 * @return bool reponse true(l'opération a réussi)/false(l'opération a raté)
 */
function DeleteKeyWord($idKeyWord)
{
    // création de la transaction car on travaille sur deux tables et que les deux doivent êtres modifiées 
    $bd = connexion();
    $bd->beginTransaction();

    connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
    $poste = connexion()->prepare('DELETE FROM operationhasmotsclef WHERE motsClefs_id = ' . $idKeyWord);
    //supression des mots clés dans un premier temps
    if ($poste->execute()) {
        connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
        $poste = connexion()->prepare('DELETE FROM motsclefs WHERE id = ' . $idKeyWord);
        //supression des relations dans un second
        if ($poste->execute()) {
            $bd->commit();
            return true;
        } else {
            $bd->rollBack();
            return false;
        }
    } else {
        $bd->rollBack();
        return false;
    }
}
/** Fonction qui suprimme un compte depuis son id ainsi que les opérations qui lui sont assignées
 * @param int $idAccount id du compte en question
 * @return bool reponse true(l'opération a réussi)/false(l'opération a raté)
 */
function DeleteAccount($idAccount)
{
    // création de la transaction car on travaille sur deux tables et que les deux doivent êtres modifiées 
    $bd = connexion();
    $bd->beginTransaction();

    connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
    $poste = connexion()->prepare('DELETE FROM operations WHERE comptes_id = ' . $idAccount);
    //supression des operations dans un premier temps
    if ($poste->execute()) {
        connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
        $poste = connexion()->prepare('DELETE FROM comptes WHERE id = ' . $idAccount);
        //supression des comptes dans un secon temps
        if ($poste->execute()) {
            $bd->commit();
            return true;
        } else {
            $bd->rollBack();
            return false;
        }
    } else {
        $bd->rollBack();
        return false;
    }
}
/** Fonction qui suprimme un budget depuis son id ainsi que les opérations qui lui sont assignées
 * @param int $idBudget id du budget en question
 * @return bool reponse true(l'opération a réussi)/false(l'opération a raté)
 */
function DeleteBudget($idBudget)
{
    // création de la transaction car on travaille sur deux tables et que les deux doivent êtres modifiées 
    $bd = connexion();
    $bd->beginTransaction();

    connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
    $poste = connexion()->prepare('UPDATE operations SET budget_id = 0 WHERE budget_id = ' . $idBudget);
    //supression des opérations dans un premier temps
    if ($poste->execute()) {
        connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
        $poste = connexion()->prepare('DELETE FROM budgets WHERE id = ' . $idBudget);
        //supression des budgets dans un premier temps
        if ($poste->execute()) {
            $bd->commit();
            return true;
        } else {
            $bd->rollBack();
            return false;
        }
    } else {
        $bd->rollBack();
        return false;
    }
}
/** Fonction qui suprimme une opération depuis son id ainsi que les liaisons de mots clés qui lui sont assigné
 * @param int $idOperation id de l'opération en question
 * @return bool reponse true(l'opération a réussi)/false(l'opération a raté)
 */
function DeleteOperation($idOperation)
{
    // création de la transaction car on travaille sur deux tables et que les deux doivent êtres modifiées 
    $bd = connexion();
    $bd->beginTransaction();

    connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
    $poste = connexion()->prepare('DELETE FROM operationhasmotsclef WHERE operations_id = ' . $idOperation);
    //supression des liaisons dans un premier temps
    if ($poste->execute()) {
        connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
        $poste = connexion()->prepare('DELETE FROM operations WHERE id = ' . $idOperation);
        //supression des operations dans un second temps
        if ($poste->execute()) {
            $bd->commit();
            return true;
        } else {
            $bd->rollBack();
            return false;
        }
    } else {
        $bd->rollBack();
        return false;
    }
}

//     _____  _____________  ______
//    /  _/ |/ / __/ __/ _ \/_  __/
//   _/ //    /\ \/ _// , _/ / /   
//  /___/_/|_/___/___/_/|_| /_/    
//           
//  Fonctions qui effectuent des requêtes d'insertion dans la base de donnée   

/** Fonction qui crée la liaison entre un mot clé et une opération donnée
 * @param int $idOperation id de l'opération en question
 * @param Array $keys tableau de 0 a plusieurs mots clés
 * @return bool reponse true(l'opération a réussi)/false(l'opération a raté)
 */
function CreateLinkOperationKeys($idOperation, $keys)
{
    // création de la transaction car on travaille avec plusieurs requêtes
    $bd = connexion();
    $bd->beginTransaction();

    connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
    $requette = connexion()->prepare("DELETE FROM operationhasmotsclef WHERE operations_id = $idOperation");
    //supression de toutes les liaisons qui pourraient déja exister concernant l'operation dans un premier temps
    //ceci car on utilise cette donction quand on crée des liaisons mais également quand on en modifie 
    if ($requette->execute()) {
        foreach ($keys as $k) {
            $insert = connexion()->prepare('INSERT INTO operationhasmotsclef VALUES (' . $idOperation . ',' . $k . ')');
            $insert->execute();
            //insertion des différentes liaisons ensuite
        }
        $bd->commit();
        return true;
    } else {
        $bd->rollBack();
        return false;
    }
}
/** Fonction qui crée un mot clé dans la base de donnée
 * @param string $name nom du mot clé
 * @return bool reponse true(l'opération a réussi)/false(l'opération a raté)
 */
function CreateKeyWord($name)
{
    connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
    $poste = connexion()->prepare('INSERT INTO motsclefs VALUES (null,"' . $name . '")');
    if ($poste->execute()) {
        return true;
    } else {
        return false;
    }
}
/** Fonction qui crée un utilisateur dans la base de donnée
 * @param string $email email de l'utilisateur (son login)
 * @param string $password mot de passe hashé de l'utilisateur ex:$2y$10$.vGA1O9wmRjrwAVXD98HNOgsNpDczlqm3Jq7KnEd1rVAGv3Fykk1a
 * @return bool reponse true(l'opération a réussi)/false(l'opération a raté)
 */
function InsertNewUser($email, $password)
{
    connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
    $poste = connexion()->prepare('INSERT INTO utilisateurs VALUES (null,"' . $email . '","' . $password . '","0")');
    if ($poste->execute()) {
        return true;
    } else {
        return false;
    }
}
/** Fonction qui crée un compte dans la base de donnée 
 * @param string $name nom du compte
 * @param int $number numéro de compte 
 * @param string $bank nom de la banque
 * @param int $userId id du user
 * @return bool reponse true(l'opération a réussi)/false(l'opération a raté)
 */
function InsertNewAccount($name, $number, $bank, $userId)
{
    connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
    $Account = connexion()->prepare('INSERT INTO comptes VALUES (null,"' . $name . '","' . $number . '","' . $bank . '","' . $userId . '")');
    if ($Account->execute()) {
        return true;
    } else {
        return false;
    }
}
/** Fonction qui crée un budget dans la base de donnée
 * @param string $name nom du budget
 * @param int $amount montant mensuel
 * @param int $userId id du user qui as créé un budget
 * @return bool reponse true(l'opération a réussi)/false(l'opération a raté)
 */
function InsertNewBudget($name, $amount, $userId)
{
    connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
    $Budget = connexion()->prepare('INSERT INTO budgets VALUES (null,"' . $name . '","' . $amount . '","' . $userId . '")');
    if ($Budget->execute()) {
        return true;
    } else {
        return false;
    }
}
/** Fonction qui crée une opération dans la base de donnée
 * @param string $name nom de l'opération
 * @param int $amount montant mensuel du budget
 * @param int $account id du compte (obligatoire)
 * @param int $budget id du budget (facultatif)
 * @return bool reponse true(l'opération a réussi)/false(l'opération a raté)
 */
function InsertNewOperation($name, $amount, $account, $budget)
{
    connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
    if ($budget == 0 || $budget == "") {
        $operation = connexion()->prepare('INSERT INTO operations VALUES (null,"' . date("Y.m.d") . '","' . $name . '","' . $amount . '","' . $account . '",NULL)');
    } else {
        $operation = connexion()->prepare('INSERT INTO operations VALUES (null,"' . date("Y.m.d") . '","' . $name . '","' . $amount . '","' . $account . '","' . $budget . '")');
    }
    //var_dump($operation);
    if ($operation->execute()) {
        return true;
    } else {
        return false;
    }
}

//    __  _____  ___  ___ __________
//   / / / / _ \/ _ \/ _ /_  __/ __/
//  / /_/ / ___/ // / __ |/ / / _/  
//  \____/_/  /____/_/ |_/_/ /___/  
//           
//  Requêtes de mise a jour dans la base de donnée

/** Fonction qui modifie un compte dans la base de donnée
 * @param string $name nom du compte
 * @param int $number numéro de compte 
 * @param string $bank nom de la banque
 * @param int $idAccount id du compte en question
 * @return bool reponse true(l'opération a réussi)/false(l'opération a raté)
 */
function ModifyAccount($name, $number, $bank, $idAccount)
{
    connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
    $Account = connexion()->prepare('UPDATE comptes SET libele = "' . $name . '",numero = "' . $number . '",banque = "' . $bank . '" WHERE id = ' . $idAccount);
    if ($Account->execute()) {
        return true;
    } else {
        return false;
    }
}
/** Fonction qui modifie un budget dans la base de donnée
 * @param string $name nom du budget
 * @param int $amount montant mensuel
 * @param int $idBudget id du buget en question
 * @return bool reponse true(l'opération a réussi)/false(l'opération a raté)
 */
function ModifyBudget($name, $amount, $idBudget)
{
    connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
    $Account = connexion()->prepare('UPDATE budgets SET titre = "' . $name . '",montant_mensuel = "' . $amount . '" WHERE id = ' . $idBudget);
    if ($Account->execute()) {
        return true;
    } else {
        return false;
    }
}
/** Fonction qui modifie une opération
 * @param string $name nom de l'opération
 * @param int $amount montant mensuel du budget
 * @param int $account id du compte (obligatoire)
 * @param int $budget id du budget (facultatif)
 * @param int $idOperation id de l'opération en question
 * @param Array $keys tableau d'ids de mots clés (entre 0 et plusieurs mots clés)
 * @return bool reponse true(l'opération a réussi)/false(l'opération a raté)
 */
function ModifyOperation($name, $amount, $idAccount, $idBudget, $idOperation, $keys, $idUser)
{
    //il faut vérifier que le compte et ou le budget appartiens bien au user

    connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
    if ($idBudget == 0 || $idBudget == "") {
        if (CheckAccountByUser($idUser, $idAccount)) {
            $Operation = connexion()->prepare('UPDATE operations SET libele = "' . $name . '",montant = "' . $amount . '" ,comptes_id = "' . $idAccount . '",budget_id = NULL WHERE id = ' . $idOperation);
        } else {
            return false;
        }
    } else {
        if (CheckAccountByUser($idUser, $idAccount)) {
            if (CheckBudgetByUser($idUser, $idBudget)) {
                $Operation = connexion()->prepare('UPDATE operations SET libele = "' . $name . '",montant = "' . $amount . '" ,comptes_id = "' . $idAccount . '",budget_id = "' . $idBudget . '" WHERE id = ' . $idOperation);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    //modification de l'opération dans un premier temps
    if ($keys != -1) {
        if ($Operation->execute()) {
            if (CreateLinkOperationKeys($idOperation, $keys)) {
                //modification des relations dans un second temps 
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    } else {
        if ($Operation->execute()) {
            return true;
        } else {
            return false;
        }
    }
}
/** Fonction qui crée un mot clé dans la base de donné
 * @param int $idKeyWord id du mot clé en question
 * @param string $name nom du mot clé
 * @return bool reponse true(l'opération a réussi)/false(l'opération a raté)
 */
function ModifyKeyWord($idKeyWord, $name)
{
    connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
    $Account = connexion()->prepare('UPDATE motsclefs SET libele = "' . $name . '" WHERE id = ' . $idKeyWord);
    if ($Account->execute()) {
        return true;
    } else {
        return false;
    }
}

//    ____  ___  _______  ___ ______________  _  ______
//   / __ \/ _ \/ __/ _ \/ _ /_  __/  _/ __ \/ |/ / __/
//  / /_/ / ___/ _// , _/ __ |/ / _/ // /_/ /    /\ \  
//  \____/_/  /___/_/|_/_/ |_/_/ /___/\____/_/|_/___/  
//              
//  Fonctions qui effectuent des opérations sur des données, des formatages etc...

/** Fonction qui vas vérifier si un user et un mot de passes sont corrects
 * @param string $email email du user
 * @param string $password mot de passe du user
 * @return bool reponse true(les identifiants sont corrects)/false(les identifiants sont erronés)
 */
function CompareLogin($email, $password)
{
    $return[0] = 0;
    $return[1] = 0;
    $return[2] = 0;
    $user = GetUserByEmail($email);
    //recupération des infos user 
    if (isset($user[0])) {
        if (password_verify($password, $user[0]["password"])) {
            //utilisation de la methode de php password_verify
            $return[0] = $user[0]["est_administrateur"];
            $return[1] = $user[0]["id"];
            $return[2] = 1;
            return $return;
        } else {
            $return[2] = 0;
            return $return;
        }
    }
}
/** Fonction de création de user
 * @param string $email email du user
 * @param string $password password en clair du user ex:Super2016
 * @return bool reponse true(l'opération a réussi)/false(l'opération a raté)
 */
function CreateUser($email, $password)
{
    $pass = password_hash($password, PASSWORD_DEFAULT);
    //utilisation de la méthode php password_hash avec l'algorythme par défaut , exemple de sortie : $2y$10$.vGA1O9wmRjrwAVXD98HNOgsNpDczlqm3Jq7KnEd1rVAGv3Fykk1a
    if (InsertNewUser($email, $pass)) {
        return true;
    } else {
        return false;
    }
}
/** Fonction qui crée un compte 
 * @param string $name nom du compte
 * @param int $number numéro de compte 
 * @param string $bank nom de la banque
 * @param int $userId id du user
 * @return bool reponse true(l'opération a réussi)/false(l'opération a raté)
 */
function CreateAccount($name, $number, $bank, $userId)
{
    if (InsertNewAccount($name, $number, $bank, $userId)) {
        return true;
    } else {
        return false;
    }
}
/** Fonction qui crée un budget
 * @param string $name nom du budget
 * @param int $amount montant mensuel
 * @param int $userId id du user qui as créé un budget
 * @return bool reponse true(l'opération a réussi)/false(l'opération a raté)
 */
function CreateBudget($name, $amount, $userId)
{
    if (InsertNewBudget($name, $amount, $userId)) {
        return true;
    } else {
        return false;
    }
}
/** Fonction qui crée une opération dans la base de donnée
 * @param string $name nom de l'opération
 * @param int $amount montant mensuel du budget
 * @param int $account id du compte (obligatoire)
 * @param int $budget id du budget (facultatif)
 * @param Array $keys tableau de mots clés !!! Important !!! si aucun mots clés donnez -1
 * @return bool reponse true(l'opération a réussi)/false(l'opération a raté)
 */
function CreateOperation($name, $amout, $account, $budget, $keys, $idUser)
{
    if($budget != null){
        if (CheckBudgetByUser($idUser, $budget)) {
            if (CheckAccountByUser($idUser, $account)) {
                if ($keys != -1) {
                    if (InsertNewOperation($name, $amout, $account, $budget)) {
                        connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
                        $poste = connexion()->prepare('SELECT max(id) FROM operations');
                        $poste->execute();
                        $lastId = $poste->fetch();
                        //LastInsertId pour des raisons inconnues ne fonctionnait pas alors utilisation de la requete max()
                        if ($lastId != null) {
                            if (CreateLinkOperationKeys($lastId[0], $keys)) {
                                return true;
                            } else {
                                return false;
                            }
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                } else {
                    if (InsertNewOperation($name, $amout, $account, $budget)) {
                        return true;
                    } else {
                        
                        return false;
                    }
                }
            } else {
                return false;
            }
        } else {
            //var_Dump($budget);
            return false;
        }
    }else{
        if (CheckAccountByUser($idUser, $account)) {
            if ($keys != -1) {
                if (InsertNewOperation($name, $amout, $account, $budget)) {
                    connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
                    $poste = connexion()->prepare('SELECT max(id) FROM operations');
                    $poste->execute();
                    $lastId = $poste->fetch();
                    //LastInsertId pour des raisons inconnues ne fonctionnait pas alors utilisation de la requete max()
                    if ($lastId != null) {
                        if (CreateLinkOperationKeys($lastId[0], $keys)) {
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                if (InsertNewOperation($name, $amout, $account, $budget)) {
                    return true;
                } else {
                    
                    return false;
                }
            }
        } else {
            return false;
        }
    }
    
}
/**
 * Fonction qui vérifie si un email existe deja ou non
 * @param string $email email a vérifier
 * @return bool true(l'utilisateur existe)/false(l'utilisateur n'existe pas)
 */
function Exist($email)
{
    $user = GetUserByEmail($email);
    if ($user != null) {
        return true;
    } else {
        return false;
    }
}
/**
 * Fonction qui vérifie si un utilisateur possède bien le compte donnée (utile pour savoir si il a accès au crude de ce dernier)
 * @param int $idUser id de l'utilisateur demandant
 * @param int $idAccount id du compte demandé
 * @return bool true(l'utilisateur est autorisé)/false(l'utilisateur n'est pas autorisé)
 */
function isAllowedCompte($idUser, $idAccount)
{
    $result = GetIfAccountAnduserMatch($idAccount, $idUser);
    if ($result != null) {
        return $result;
    } else {
        return null;
    }
}
/**
 * Fonction qui vérifie si un utilisateur possède bien le budget donné (utile pour savoir si il a accès au crude de ce dernier)
 * @param int $idUser id de l'utilisateur demandant
 * @param int $idBudget id du budget demandé
 * @return bool true(l'utilisateur est autorisé)/false(l'utilisateur n'est pas autorisé)
 */
function isAllowedBudget($idUser, $idBudget)
{
    $result = GetIfBudgetAnduserMatch($idBudget, $idUser);
    if ($result != null) {
        return $result;
    } else {
        return null;
    }
}
/**
 * Fonction qui vérifie si un user as bien crée l'opération donéé
 * @param int $idUser id de l'utilisateur demandant
 * @param int $idOperation id de lôpération demandée
 * @return bool true(l'utilisateur est autorisé)/false(l'utilisateur n'est pas autorisé)
 */
function isUserAllowedOperation($idOperation, $idUser)
{
    $id = GetIdUserByOperation($idOperation);
    if ($id != null) {
        if ($id == $idUser) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

//     ___  ___________  __   _____  __
//    / _ \/  _/ __/ _ \/ /  / _ \ \/ /
//   / // // /_\ \/ ___/ /__/ __ |\  / 
//  /____/___/___/_/  /____/_/ |_|/_/  
//                                     
//  Fonctions qui vont effectuer des formatages d'html pour l'affichage de données dans les pages 

/**
 * Fonction qui vas afficher les différents comptes du user dans le dashboard
 * @param int $idUser id du user concerné
 * @return bool true("il y a des comptes sur cete utilisateur")/false("cet utilisateur n'as aucun compte")
 */
function DisplayAccountsByUser($idUser)
{
    $accounts = GetAllAccountsByUser($idUser);
    //récuperation de toutes les infos des comptes du user
    if ($accounts != null) {
        $count = Count($accounts);
        //affichage en colonne et lignes de bootstrap
        echo '<div class="row" style="margin-left:5%;margin-right:5%">';
        $i = 0;
        foreach ($accounts as $a) {
            if ($i < 5) {
                //affichage adaptatif au nombre de comptes
                if ($count < 6) {
                    echo '<div class="col-sm-' . (12 / $count) . '" >';
                } else {
                    echo '<div class="col-sm-' . (12 / 2) . '" >';
                }
                //creation de la carte de compte , l'endroit ou seront affichées toutes les infos de compte
                echo '<div class="card">';
                echo '<div class="card-body text-center" style="background-color:#212121;">';
                echo '<h3 class="card-title">';
                //bouton de modification qui redirige sur modifCompte.php avec l'id du compte en get
                echo '<a href="modifCompte.php?id=' . $a["id"] . '" class="btn btn-warning" style="margin-right:5%;background-color:#F2994A;color:white"><i class="far fa-edit"></i></a>';
                //affichage du nom du compte , de son libele
                echo $a["libele"];
                //bouton de supression du compte qui redirige vers la page deleteCompte.hp avec l'id du compte en get
                echo '<a href="deleteCompte.php?id=' . $a["id"] . '" class="btn btn-danger" style="margin-left:5%;background-color:#EB5757;color:white"><i class="far fa-trash-alt"></i></a>';
                echo '</h3>';
                //récupère la somme de toutes les opérations sur le compte pour en avoir son état actuel , sa balance
                $balance = GetSommeOnAccount($a["id"])[0]["SUM(montant)"];
                if ($balance != null) {
                    if ($balance < 0) {
                        //si le compte est dans le rouge
                        echo '<h1 class="card-text" style="color:#EB5757">' . $balance . ' CHF</h1>';
                    } else {
                        //si le compte est dans le vert
                        echo '<h1 class="card-text" style="color:#219653">' . $balance . ' CHF</h1>';
                    }
                } else {
                    //si aucune opération n'as été faite sur le compte
                    echo '<h1 class="card-text" style="color:#EB5757">0</h1>';
                }
                echo "</div>";
                echo "</div>";
                echo "</div>";
            }
            $i++;
        }
        echo '</div>';
        if (Count($accounts) >= 5) {
            echo '<div class="text-center" style="margin-top:1%"><a href="ajoutCompte.php" style="color:#AAAAAA;text-decoration:none"><button type="button" class="btn btn-light">Voir plus</button></a></div>';
            echo '';
        }

        return true;
    } else {
        return false;
    }
}
/**
 * Fonction qui vas afficher les différents comptes du user dans la page comptes
 * @param int $idUser id du user concerné
 */
function DisplayAccountsByUserFull($idUser)
{
    //pour plus d'info sur la mise en page voir la fonction DisplayAccountByUser()
    $accounts = GetAllAccountsByUser($idUser);
    //récuperation de toutes les infos des comptes du user
    if ($accounts != null) {
        $count = Count($accounts);
        echo '<div class="row" style="margin-left:5%;margin-right:5%">';
        $i = 0;
        foreach ($accounts as $a) {
            $operations = getAllOperationsAsAccount($a["id"]);
            //récupèration des opérations du compte et leur détails
            if ($i < 5) {
                echo '<div class="col-sm-' . 12 . '" style="margin-bottom:1%">';
                //la carte prends directement toute la largeur d la page car on veut son détail et c'est plus lisible de cette façon
                echo '<div class="card">';
                echo '<div class="card-body text-center" style="background-color:#212121;">';
                echo '<h3 class="card-title">';
                echo '<a href="modifCompte.php?id=' . $a["id"] . '" class="btn btn-warning" style="margin-right:5%;background-color:#F2994A;color:white"><i class="far fa-edit"></i></i></a>';
                echo $a["libele"];
                echo '<a href="deleteCompte.php?id=' . $a["id"] . '" class="btn btn-danger" style="margin-left:5%;background-color:#EB5757;color:white"><i class="far fa-trash-alt"></i></a>';
                echo '</h3>';
                $balance = GetSommeOnAccount($a["id"])[0]["SUM(montant)"];
                //récupèration de la balance du compte
                if ($balance != null) {
                    if ($balance < 0) {
                        echo '<h1 class="card-text" style="color:#EB5757">' . $balance . ' CHF</h1>';
                    } else {
                        echo '<h1 class="card-text" style="color:#219653">' . $balance . ' CHF</h1>';
                    }
                } else {
                    echo '<h1 class="card-text" style="color:#EB5757">0</h1>';
                }
                echo '<table class="table table-hover" style="background-color:#3D3D3D;color:#AAAAAA">';
                echo '<thead>';
                echo '<tr>';
                //titres de tableaux
                echo '<th scope="col">#</th>';
                echo '<th scope="col">Motif</th>';
                echo '<th scope="col">Montant</th>';
                echo '<th scope="col">Date</th>';
                echo '<th scope="col">Operation</th>';
                echo '</tr>';
                echo '</thead>';
                echo '<tbody>';
                if ($operations != null) {
                    foreach ($operations as $o) {
                        echo '<tr>';
                        echo '<th scope="row" >' . $o["id"] . '</th>';
                        echo '<td>' . $o["libele"] . '</td>';
                        //Affichage du montant et de la date de chaque opération ainsi que les autres informations 
                        if ($o["montant"] < 0) {
                            echo '<td style="color:#EB5757">' . $o["montant"] . '</td>';
                        } else {
                            echo '<td style="color:#219653">' . $o["montant"] . '</td>';
                        }
                        //echo '<td>' . $o["montant"] . '</td>';
                        echo '<td>' . $o["date"] . '</td>';
                        echo '<td><a href="modifoperation.php?id=' . $o["id"] . '" class="btn btn-warning" style="margin-right:5%;background-color:#F2994A;color:white"><i class="far fa-edit"></i></a><a href="deleteOperation.php?id=' . $o["id"] . '" class="btn btn-danger" style="margin-left:5%;background-color:#EB5757;color:white"><i class="far fa-trash-alt"></i></a></td>';
                        echo '</tr>';
                    }
                } else {
                    echo '<tr>';
                    echo '<th scope="row" colspan="3"> No Data </th>';
                    echo '</tr>';
                }
                echo '</tbody>';
                echo '</table>';
                echo "</div>";
                echo "</div>";
                echo "Banque : ";
                echo $a["banque"];
                echo " Numero: ";
                echo $a["numero"];
                echo "</div>";
            }
            $i++;
        }
        echo '</div>';
        if (Count($accounts) >= 5) {
            echo '<div class="text-center" style="margin-top:1%"><a href="ajoutCompte.php" style="color:#AAAAAA;text-decoration:none"><button type="button" class="btn btn-light">Voir plus</button></a></div>';
            echo '';
        }
    } else {

        //echo "<h2 class='text-center'>Aucun Comptes</h2>";
    }
}
/**
 * Fonction qui affiche sur la page de dashboard les infos des budgets du user
 * @param int $idUser id du user en question
 * @return bool true(l'utilisateur as bien des budgets)/false(l'utilisateur n'as pas de budgets)
 */
function DisplayAllBudgetsByUser($idUser)
{
    //pour plus d'info sur la mise en page voir DisplayAllAccountByUser()
    $budgets = GetAllBudgetsByUser($idUser);
    //récupèreation des infos des budgets selon le user
    if ($budgets != null) {
        $count = Count($budgets);
        echo '<div class="row" style="margin-left:5%;margin-right:5%">';
        foreach ($budgets as $b) {
            if ($count < 6) {
                echo '<div class="col-sm-' . (12 / $count) . '" >';
            } else {
                echo '<div class="col-sm-' . (12 / 2) . '" >';
            }
            echo '<div class="card">';
            echo '<div class="card-body text-center" style="background-color:#212121;">';
            echo '<h3 class="card-title">';
            echo '<a href="modifBudget.php?id=' . $b["id"] . '" class="btn btn-warning" style="margin-right:5%;background-color:#F2994A;color:white"><i class="far fa-edit"></i></a>';
            echo $b["titre"];
            echo '<a href="deleteBudget.php?id=' . $b["id"] . '" class="btn btn-danger" style="margin-left:5%;background-color:#EB5757;color:white"><i class="far fa-trash-alt"></i></a>';
            echo '</h3>';
            $month = Date("n"); //format 5 au lieu de 05
            $total = GetSommeOnBudget($b["id"], $month)[0]["SUM(montant)"];
            //récuperation de la somme des opérations sur un budget sur le mois en cour
            if ($total + $b["montant_mensuel"] < 0) {
                if ($total < 0) {
                    $total = abs($total);
                }
                echo '<h1 class="card-text" style="color:#EB5757">-' . $total . "/" . $b["montant_mensuel"] . '</h1>';
            } else {
                if ($total > 0) {
                    echo '<h1 class="card-text" style="color:#219653">+' . $total . "/" . $b["montant_mensuel"] . '</h1>';
                } else {
                    echo '<h1 class="card-text" style="color:#219653">' . $total . "/" . $b["montant_mensuel"] . '</h1>';
                }
            }
            echo '<table class="table table-hover" style="background-color:#3D3D3D;color:#AAAAAA">';
            echo '<thead>';
            echo '<tr>';
            echo '<th scope="col">Mois</th>';
            echo '<th scope="col">Montant Mensuel</th>';
            echo '<th scope="col">Valeur</th>';
            echo '</tr>';
            echo '</thead>';
            echo '<tbody>';
            $Results = GetOperationsFromBudgetWithMonth(6, $b["id"]);
            //récupèration de la somme des budgets des 6 derniers mois groupé par mois
            foreach ($Results as $r) {
                echo '<tr scope="col">';
                //methode trouvée sur internet pour convertir un numéro en mois de l'année
                $dateObj   = DateTime::createFromFormat('!m', $r["MONTH(date)"]);
                $monthName = $dateObj->format('F'); // March
                //Convertir le mois en francais n'as pas encore été implémenté
                echo '<td>' . $monthName . '</td>';
                echo '<td>' . $b["montant_mensuel"] . '</td>';
                if ($r["SUM(montant)"] <= -$b["montant_mensuel"] || $r["SUM(montant)"] == 0) {
                    echo '<td style="color:#EB5757">' . $r["SUM(montant)"] . '</td>';
                } else {
                    echo '<td style="color:#219653">' . $r["SUM(montant)"] . '</td>';
                }
                echo '</tr>';
            }
            echo "<td></td>";
            echo "<td></td>";
            echo "<td></td>";
            echo '</tbody>';
            echo '</table>';
            echo "</div>";
            echo "</div>";
            echo "</div>";
        }
        echo '</div>';
        return true;
    } else {
        return false;
    }
}
/**
 * Fonction qui affiche sur la page de budgets les infos des budgets du user
 * @param int $idUser id du user en question
 */
function DisplayAllBudgetsByUserFull($idUser)
{
    //pour plus d'info sur la mise en page voir DisplayAllAccountByUser()
    $budgets = GetAllBudgetsByUser($idUser);
    //récupèration des infos des budgets du user 
    if ($budgets != null) {
        $count = Count($budgets);
        echo '<div class="row" style="margin-left:5%;margin-right:5%">';
        foreach ($budgets as $b) {
            $operations = getAllOperationsASBudgetThisMonth($b["id"]);
            //récupèration de toutes les opérations du mois en cour
            echo '<div class="col-sm-' . 12 . '" >';
            echo '<div class="card">';
            echo '<div class="card-body text-center" style="background-color:#212121;">';
            echo '<h3 class="card-title">';
            echo '<a href="modifBudget.php?id=' . $b["id"] . '" class="btn btn-warning" style="margin-right:5%;background-color:#F2994A;color:white"><i class="far fa-edit"></i></a>';
            echo $b["titre"];
            echo '<a href="deleteBudget.php?id=' . $b["id"] . '" class="btn btn-danger" style="margin-left:5%;background-color:#EB5757;color:white"><i class="far fa-trash-alt"></i></a>';
            echo '</h3>';
            $month = Date("m") * 2 / 2;
            $total = GetSommeOnBudget($b["id"], $month)[0]["SUM(montant)"];
            //récupèration de la somme de toutes les opérations sur le budget ce mois ci
            if ($total < -$b["montant_mensuel"]) {
                echo '<h1 class="card-text" style="color:#EB5757">' . $total . "/" . $b["montant_mensuel"] . '</h1>';
            } else {
                echo '<h1 class="card-text" style="color:#219653">' . $total . "/" . $b["montant_mensuel"] . '</h1>';
            }
            echo '<table class="table table-hover" style="background-color:#3D3D3D;color:#AAAAAA">';
            echo '<thead>';
            echo '<tr>';
            echo '<th scope="col">#</th>';
            echo '<th scope="col">Motif</th>';
            echo '<th scope="col">Montant</th>';
            echo '<th scope="col">Date</th>';
            echo '<th scope="col">Actions</th>';
            echo '</tr>';
            echo '</thead>';
            echo '<tbody>';
            if ($operations != null) {
                foreach ($operations as $o) {
                    echo '<tr>';
                    echo '<th scope="row" >' . $o["id"] . '</th>';
                    echo '<td>' . $o["libele"] . '</td>';
                    echo '<td>' . $o["montant"] . '</td>';
                    echo '<td>' . $o["date"] . '</td>';
                    echo '<td><a href="modifoperation.php?id=' . $o["id"] . '" class="btn btn-warning" style="margin-right:5%;background-color:#F2994A;color:white"><i class="far fa-edit"></i></a><a href="deleteOperation.php?id=' . $o["id"] . '" class="btn btn-danger" style="margin-left:5%;background-color:#EB5757;color:white"><i class="far fa-trash-alt"></i></i></a></td>';
                    echo '</tr>';
                }
            } else {
                echo '<tr>';
                echo '<th scope="row" colspan="3"> No Data </th>';
                echo '</tr>';
            }
            echo '</tbody>';
            echo '</table>';
            echo "</div>";
            echo "</div>";
            echo "</div>";
            echo "<div style='margin-bottom:1%'></div>";
        }
        echo '</div>';
    } else {
        //aucune action
    }
}
/**
 * Fonction qui affiche sur la page des opérations ou de dashboard les derniers opérations faites par un user
 * @param int $idUser id du user en question
 * @param $limit la quantité d'opération que l'on veut voir s'afficher
 */
function DisplayOperationsByUser($idUser, $limit)
{
    $operations = GetAllOperationsByUser($idUser, $limit);

    //récupèration de toutes les opérations du user dans la limite demandée
    if ($operations != null) {
        echo '<div class="row text-center border" style="margin-left:5%;margin-right:5%">';
        //création du tableau
        echo '<div class="col-sm-12">';
        echo '<table class="table table-hover" style="background-color:#212121;color:#AAAAAA">';
        echo '<thead>';
        echo '<tr>';
        //titres de tableau
        echo '<th scope="col">#</th>';
        //oui on montre l'id de l'opération car cela peut aider en cas de soucis en pouvant donner un id a l'administrateur
        echo '<th scope="col">Motif</th>';
        echo '<th scope="col">Montant</th>';
        echo '<th scope="col">Date</th>';
        echo '<th scope="col">Gèrer</th>';
        echo '<th scope="col">mots clés</th>';
        echo '</tr>';
        echo '</thead>';
        echo '<tbody>';
        foreach ($operations as $o) {
            $keyWords = GetKeyWordByOperation($o["id"]);
            //récupèrations des mots clés de chaque operation
            echo '<tr>';
            echo '<th scope="row" style="color:white">' . $o["id"] . '</th>';
            echo '<td style="color:white">' . $o["libele"] . '</td>';
            //affichage du libele de l'operation
            if ($o["montant"] < 0) {
                echo '<td style="color:#EB5757">' . $o["montant"] . '</td>';
            } else {
                echo '<td style="color:#219653">' . $o["montant"] . '</td>';
            }
            //affichage du montant de l'opération
            echo '<td style="color:white">' . $o["date"] . '</td>';
            //affichage de la date de l'opération
            echo '<td><a href="modifoperation.php?id=' . $o["id"] . '" class="btn btn-warning" style="margin-right:5%;background-color:#F2994A;color:white"><i class="far fa-edit"></i></a><a href="deleteOperation.php?id=' . $o["id"] . '" class="btn btn-danger" style="margin-left:5%;background-color:#EB5757;color:white"><i class="far fa-trash-alt"></i></i></a></td>';
            echo '<td>';
            foreach ($keyWords as $k) {
                echo '<span class="badge bg-secondary" style="color:white;margin:1%">' . $k["libele"] . '</span>';
            }
            echo '</td>';
            //boutons de modification et suppression
            echo '</tr>';
        }
        echo '</tbody>';
        echo '</table>';
        echo '</div>';
        echo '</div>';
    }
}
/**
 * Fonction qui affiche les différents mots clés pour la page admin
 */
function DisplayKeyWords()
{
    $keyWords = GetKeyWords();
    //récupèration de tous les mots clés
    if ($keyWords != null) {
        echo '<div class="row text-center" style="margin-left:5%;margin-right:5%">';
        //création du tableau
        echo '<table class="table table-hover border" style="background-color:#212121;color:#AAAAAA">';
        echo '<thead>';
        echo '<tr>';
        //entêtes de tableau
        echo '<th scope="col">#</th>';
        echo '<th scope="col">Mot clef</th>';
        echo '<th scope="col">Actions</th>';
        echo '</tr>';
        echo '</thead>';
        echo '<tbody>';
        foreach ($keyWords as $k) {
            echo '<tr>';
            //affichage de l'id du mot clé
            echo '<th scope="row" >' . $k["id"] . '</th>';
            //affichage du libele du mot clé
            echo '<td>' . $k["libele"] . '</td>';
            //creation des différens boutons de modification et de supession
            echo '<td><a href="modifMotClef.php?id=' . $k["id"] . '" class="btn btn-warning" style="margin-right:5%;background-color:#F2994A;color:white"><i class="far fa-trash-alt"></i></a><a href="deleteMotClef.php?id=' . $k["id"] . '" class="btn btn-danger" style="margin-left:5%;background-color:#EB5757;color:white"><i class="far fa-trash-alt"></i></a></td>';
            echo '</tr>';
        }
        echo '</tbody>';
        echo '</table>';
    } else {
        //dans le cas ou il n'y aurait aucuns mots clés
        echo "no data";
    }
}
/**
 * Fonction qui affiche les différents mots clés sur la page de dashboard utilisateur sous forme de "nuage de mots"
 */
function DisplayKeyWordCloud()
{
    $order = GetAllKeyWordIterations();
    $keys = GetKeyWords();
    $totalOcc = 0;
    $multiplyer = 0;
    foreach ($order as $o) {
        $totalOcc += $o["occ"];
    }
    if ($totalOcc != 0) {
        $multiplyer = 100 / $totalOcc;
    } else {
        $multiplyer = 0;
    }
    echo "<div class='row text-center align-items-center border rounded' style='background-color:#212121'>";
    foreach ($keys as $k) {
        foreach ($order as $o) {
            if ($k["id"] == $o["motsClefs_id"]) {
                //les mots ont une taille maximum de 100 points
                //selon leurs pourcentage d'apparition ils vont avoir une taille différente 
                //ex: un mot qui apparait 32% du temps apparaitra avec une fonte de 32pt
                echo '<span class="col" style="color:white;padding:0px;font-size:' . ($o["occ"] * $multiplyer) . 'pt">' . $k["libele"] . '</span>';
            }
        }
    }
    echo "</div>";
}

//     ___  _______  __  _______
//    / _ \/ __/ _ )/ / / / ___/
//   / // / _// _  / /_/ / (_ / 
//  /____/___/____/\____/\___/  
//      
//  Fonctions de developpement qui ne seront normalement pas utilisées dans le produit fini                        

// Fonction de debug utilisée pour le changement de format des dates
// @param Aucuns
// @return Rien
function SetupAllDates()
{
    connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
    $Account = connexion()->prepare('UPDATE operations SET date = "' . date("Y.m.d") . '"');
    $Account->execute();
    //var_dump($Account);
}
